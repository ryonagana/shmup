TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt


unix: {
    LIBS += -L/usr/lib -lallegro -lallegro_font -lallegro_ttf  -lallegro_image -lallegro_audio -lallegro_acodec -lallegro_primitives -lallegro_dialog -lallegro_physfs -lphysfs
    INCLUDEPATH += -I/usr/include
    DEPENDPATH  += /usr/include
}


HEADERS += \
    ../src/graphic/fade.h \
    ../src/input/keyboard.h \
    ../src/input/mouse.h \
    ../src/starfield/starfield.h \
    ../src/ui/ui.h \
    ../src/utils/collision.h \
    ../src/utils/debug.h \
    ../src/utils/math_helper.h \
    ../src/utils/mixer.h \
    ../src/utils/resources.h \
    ../src/utils/utils.h \
    ../src/utils/vector2.h \
    ../src/utils/xalloc.h \
    ../src/window/scene.h \
    ../src/window/window.h \
    ../src/enemy.h \
    ../src/game.h \
    ../src/player.h \
    ../src/shot.h \
    ../src/sound_res.h \
    ../src/gun.h \
    ../src/powerup.h \
    ../src/graphic/sprite.h \
    ../src/utils/game_std.h \
    ../src/gameover.h \
    ../src/utils/json.h \
    ../src/utils/text.h \
    ../src/graphic/particles.h \
    ../src/graphic/effects.h \
    ../src/img_res.h \
    ../src/input/inputkeys.h \
    ../src/utils/angles.h \
    ../src/level/tiles.h \
    ../src/editor.h \
    ../src/fsm.h \
    ../src/utils/type.h

SOURCES += \
    ../src/graphic/fade.c \
    ../src/input/keyboard.c \
    ../src/input/mouse.c \
    ../src/starfield/starfield.c \
    ../src/ui/ui.c \
    ../src/utils/collision.c \
    ../src/utils/debug.c \
    ../src/utils/math_helper.c \
    ../src/utils/mixer.c \
    ../src/utils/resources.c \
    ../src/utils/utils.c \
    ../src/utils/vector2.c \
    ../src/utils/xalloc.c \
    ../src/window/scene.c \
    ../src/window/window.c \
    ../src/enemy.c \
    ../src/game.c \
    ../src/player.c \
    ../src/shmup.c \
    ../src/shot.c \
    ../src/sound_res.c \
    ../src/gun.c \
    ../src/powerup.c \
    ../src/graphic/sprite.c \
    ../src/utils/game_std.c \
    ../src/gameover.c \
    ../src/utils/json.c \
    ../src/utils/text.c \
    ../src/graphic/particles.c \
    ../src/graphic/effects.c \
    ../src/img_res.c \
    ../src/input/inputkeys.c \
    ../src/utils/angles.c \
    ../src/level/tiles.c \
    ../src/editor.c \
    ../src/fsm.c \
    ../src/utils/type.c






