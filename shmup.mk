##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
THIS_DIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
ProjectName            :=shmup
ConfigurationName      :=Debug
WorkspacePath          := $(THIS_DIR)
ProjectPath            := $(THIS_DIR)
IntermediateDirectory  :=bin/Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Nicholas
Date                   :=18/02/18
CodeLitePath           :=/home/ryonagana/.codelite
LinkerName             :=gcc
SharedObjectLinkerName :=gcc -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.o.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=bin/Debug/shmup
RunWorkingDir          :=src
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E 
ObjectsFileList        :="shmup.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
CreateFileCommand      :=touch
LinkOptions            :=  
IncludePCH             := 
RcIncludePath          := 
IncludePath            := $(addprefix $(IncludeSwitch),\
                            ./src/libs/tmx/include \
                            . \
                        )
LibPath                 := $(addprefix $(LibraryPathSwitch),\
                            /usr/local/include \
                            ./src/libs/tmx/lib \
                            . \
                        )
Libs                    := $(addprefix $(LibrarySwitch),\
                            allegro \
                            allegro_main \
                            allegro_dialog \
                            allegro_font \
                            allegro_ttf \
                            allegro_image \
                            allegro_primitives \
                            allegro_audio \
                            allegro_acodec \
                            m \
                            physfs \
                        )

SOURCES_BLACKLIST       := $(addprefix src/,\
                            level/level.c \
                        )

HEADERS_ALL  := $(wildcard src/*.h) $(wildcard src/**/*.h)
SOURCES_ALL  := $(wildcard src/*.c) $(wildcard src/**/*.c)
SOURCES      := $(filter-out $(SOURCES_BLACKLIST),$(SOURCES_ALL))
OBJECTS      := $(SOURCES:src/%.c=$(IntermediateDirectory)/%.c$(ObjectSuffix))
OBJECTS_DIRS := $(addsuffix .d, bin/Debug/ $(dir $(OBJECTS)))

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := ar rcus
CXX      := gcc
CC       := gcc
CXXFLAGS :=  -Wextra -g -Wall  $(Preprocessors)
CFLAGS   :=  -Wall -g -Wextra -pedantic $(Preprocessors)
ASFLAGS  := 
AS       := 

##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild run
all: $(OutputFile)
PreBuild:

-include $(IntermediateDirectory)/*$(DependSuffix)

##
## Create Build directories
##
%/.d:
	@test -d $@ || ( $(MakeDirCommand) $(dir $@) && $(CreateFileCommand) $(dir $@).d )

##
## Build object files
##
$(IntermediateDirectory)/%$(ObjectSuffix): src/% $(HEADERS_ALL)
	$(CC) $(SourceSwitch) $< $(CFLAGS) $(OutputSwitch) $@ $(IncludePath)

##
## Build main executable
##
$(OutputFile): $(OBJECTS_DIRS) $(OBJECTS)
	$(LinkerName) $(OutputSwitch)$(OutputFile) $(OBJECTS) $(LibPath) $(Libs) $(LinkOptions)

##
## Clean
##
clean:
	$(RM) -r bin/Debug/

##
## Run
##
run: $(OutputFile)
	(cd $(THIS_DIR)/$(RunWorkingDir) && $(THIS_DIR)/$(OutputFile))
