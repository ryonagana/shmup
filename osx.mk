#please run this file as make -f osx.mk
#to prevent overwriting i just renamed 

.PHONY: clean All

All:
	@echo "----------Building project:[ shmup - Debug ]----------"
	@"$(MAKE)" -f  "shmup.mk"
clean:
	@echo "----------Cleaning project:[ shmup - Debug ]----------"
	@"$(MAKE)" -f  "shmup.mk" clean
run:
	@"$(MAKE)" -f  "shmup.mk" run
