#ifndef IMG_RES_HEADER
#define IMG_RES_HEADER
#include <allegro5/allegro5.h>

void img_res_init(void);
void img_res_destroy(void);
void img_res_ready_image(void);


extern ALLEGRO_BITMAP *ready_img;
extern ALLEGRO_BITMAP *go_img;


#endif
