#ifndef SCENE_HEADER
#define SCENE_HEADER

#include <stdio.h>
#include <allegro5/allegro5.h>


#define MAX_SCENES (16)

typedef enum GameSceneID {
    SCENE_MAINGAME = 0,
    SCENE_GAMEOVER,
    SCENE_CREDITS,
    SCENE_EDITOR,
    SCENE_LAST,
}GameSceneID;


typedef struct Scene {
    void (*init)(void);
    void (*start)(void);
    void (*update)(ALLEGRO_EVENT *e);
    void (*render)(void);
    void (*update_keyboard)(ALLEGRO_EVENT *e);
    void (*destroy)(void);
    GameSceneID id;
    int has_init;
    int is_paused;

}Scene;


typedef struct _sceneList {
    Scene *sceneList[SCENE_LAST + 1];
    Scene *actualScene;
    int count;
    GameSceneID index;
    Scene * lastScene;

}sceneList;

extern sceneList sList;



void scene_start(void);
void scene_insert(Scene *scene, GameSceneID index);
void scene_load( GameSceneID index);
void scene_reset(void);
void scene_reset_selected_id(GameSceneID scene);
void scene_init(void);
void scene_render(void);
void scene_update_game(ALLEGRO_EVENT *e);
void scene_update_keyboard(ALLEGRO_EVENT *e);
void scene_destroy(void);
#endif
