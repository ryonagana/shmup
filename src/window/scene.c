#include "scene.h"
#include "../utils/utils.h"
#include "window.h"
#include <allegro5/allegro_native_dialog.h>

/* window.c  */
extern ALLEGRO_EVENT_QUEUE *g_queue;
extern ALLEGRO_DISPLAY *g_display;
sceneList sList;
/* end of window.c */

extern int loopClose; // main loop variable shmup.c

void scene_start(){
    memset(&sList, 0x0, sizeof(sList));
    sList.count = 0;
    sList.index = 0;

}

void scene_reset_selected_id(GameSceneID scene){
    Scene *actual = (sList.actualScene);

    scene_load(scene);
    scene_reset();
    scene_load(actual->id);

}

void scene_reset(){
    if(sList.actualScene != NULL){
        sList.actualScene->start();
    }
}

void scene_init(){
    int i;

    for(i = 0; i < SCENE_LAST + 1 && sList.sceneList[i] != NULL && !sList.sceneList[i]->has_init; i++){
         sList.sceneList[i]->init();
         sList.sceneList[i]->start();
         sList.sceneList[i]->has_init = 1;
    }
}

void scene_insert(Scene *scene, GameSceneID index){

    if(sList.count > SCENE_LAST + 1) return;
     sList.sceneList[index] = scene;
     sList.index = index;
     sList.sceneList[index]->id = index;
     ++sList.count;
}

void scene_load(GameSceneID index){
    int i = (int) index;
    if(sList.sceneList[i] == NULL){
        al_show_native_message_box(g_display, "Error!", "Critical Error!", "No Scene Loaded..", "ok",0);
        loopClose = TRUE;
        return;
    }




    window_reset_input();

    /*
      when a new scene is loaded we need to clear the old queue to generate a new one
      can cause input problems is the queue is not dealt
      last key pressed stay in the queue and  seems it keep pressed al time
    */
    al_flush_event_queue(g_queue);


    sList.actualScene = sList.sceneList[i];



}
void scene_render(){

    if(sList.actualScene == NULL) return;

    sList.actualScene->render();
}
void scene_update_game(ALLEGRO_EVENT *e){
    if(sList.actualScene == NULL) return;
    sList.actualScene->update(e);
}

void scene_update_keyboard(ALLEGRO_EVENT *e){
    if(sList.actualScene == NULL) return;
    sList.actualScene->update_keyboard(e);
}

void scene_destroy(){
    if(sList.actualScene == NULL) return;
    sList.actualScene->destroy();
}

