#include "window.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_opengl.h>
#include <allegro5/allegro_physfs.h>
#include <allegro5/allegro_native_dialog.h>
#include <physfs.h>
#include "../utils/utils.h"
#include "../img_res.h"

#define TICKSPERFRAME 60

ALLEGRO_EVENT_QUEUE *g_queue;
ALLEGRO_DISPLAY *g_display;
ALLEGRO_KEYBOARD_STATE g_kbdstate;
ALLEGRO_BITMAP *g_screen;
ALLEGRO_TIMER *g_timer;
ALLEGRO_TIMER *timer_ingame;
ALLEGRO_BITMAP *g_screen;


#define MILLISECONDS_TIMER (0.1)


#define CFG_DEFAULT_WIDTH   800
#define CFG_DEFAULT_HEIGHT  600
#define CFG_FULLSCREEN_MODE 0
#define CFG_USE_OPENGL      0
#define CFG_USE_MOUSE       0
#define CFG_WINDOW_NAME     "Shoot'em Up!\0"
#define CFG_DEFAULT_VSYNC   0






window_cfg window_conf = {
    CFG_DEFAULT_WIDTH,
    CFG_DEFAULT_HEIGHT,
    CFG_FULLSCREEN_MODE,
    CFG_USE_OPENGL,
    CFG_USE_MOUSE,
    CFG_WINDOW_NAME,
    CFG_DEFAULT_VSYNC
};


MouseInput mInput;
window_cfg window_conf;


int64_t *game_timer_count = NULL;
int64_t *game_timer_milliseconds = NULL;

void init_phyfs(void){
    /*
        it uses physfs to load files inside a file
        it supports
        .pak files (Quake I / Quake 2)
        .grp (Build engine files as Dukenukem 3D)
        .hog (descent II)
        .zip
        .7z

        atm we gonna use zip file for UTF-8 support
    */


#ifdef WITH_PHYSFS

        if(!PHYSFS_addToSearchPath("assets.zip",1)){
            printf("STANDARD MODE FILE LOADING");
            return;
        }
#else
        al_set_standard_file_interface();
#endif



}

static int init_allegro(void) {


    if (!al_init()){

        //DEBUG_PRINT("Error: Cannot Initialize Allegro :-(");
        return -1;

    }

    if(!al_install_keyboard()){
        //DEBUG_PRINT(("Cannot Install Keyboard"));
        return -1;
    }

    if(!al_install_mouse()){
        return -1;
    }



    init_phyfs();


    al_init_image_addon();
    al_init_font_addon();
    al_init_ttf_addon();
    al_init_primitives_addon();
    al_install_audio();
    al_install_mouse();
    al_init_acodec_addon();
    al_init_native_dialog_addon();

    if(window_conf.vsync <= 0){
        al_set_new_display_option(ALLEGRO_VSYNC,2, ALLEGRO_REQUIRE);
    }

    if(window_conf.fullscreen <= 0){
        al_set_new_display_flags(ALLEGRO_WINDOWED | ALLEGRO_OPENGL_3_0 | ALLEGRO_RESIZABLE);
    }else {
        al_set_new_display_flags(ALLEGRO_FULLSCREEN | ALLEGRO_OPENGL_3_0 | ALLEGRO_RESIZABLE);
    }

    g_display =  al_create_display(window_conf.width, window_conf.height);

    int w,h;
    w = al_get_display_width(g_display);
    h = al_get_display_height(g_display);



    g_screen  =  al_create_bitmap(w,h);

    al_get_keyboard_state(&g_kbdstate);
    al_set_target_bitmap(g_screen);
    al_set_target_bitmap(al_get_backbuffer(g_display));


    g_timer = al_create_timer( (double) 1.0f / TICKSPERFRAME );


    timer_ingame = al_create_timer(MILLISECONDS_TIMER);
    g_queue = al_create_event_queue();

        al_register_event_source(g_queue, al_get_display_event_source(g_display));
    al_register_event_source(g_queue, al_get_keyboard_event_source());
    al_register_event_source(g_queue, al_get_mouse_event_source());
    al_register_event_source(g_queue, al_get_timer_event_source(g_timer));

    /* register the ingame time   when the game starts  the timing starts */
    al_register_event_source(g_queue, al_get_timer_event_source(timer_ingame));

    al_set_window_title(g_display, window_conf.window_name);

    al_start_timer(g_timer);
    al_start_timer(timer_ingame);


    al_set_target_bitmap(al_get_backbuffer(g_display));




    return 0;
}



void window_init()
{
	init_allegro();
	window_config_init();




    game_timer_milliseconds = NULL;
    game_timer_count = NULL;
    img_res_init();

    game_timer_count =   malloc(sizeof(int64_t));
    game_timer_milliseconds = malloc(sizeof(int64_t));

    /* init mouse input */
    mouse_init(&mInput);






}
void window_close()
{

    if(g_queue) 		al_destroy_event_queue(g_queue);
	if(g_display) 		al_destroy_display(g_display);
	if(g_screen)  		al_destroy_bitmap(g_screen);
	if(g_timer)   		al_destroy_timer(g_timer);
	if(timer_ingame)  al_destroy_timer(timer_ingame);





    xfree((void*)(&game_timer_count) );
    xfree((void*)(&game_timer_milliseconds) );
    game_timer_milliseconds = 0x0;
    game_timer_count = 0x0;

    img_res_destroy();



}



void window_config_init(void)
{


    FILE *fp = fopen("wincfg.dat", "rb");

    if(fp == NULL){
        window_config_save("wincfg.dat");
    }
    fclose(fp);

    /*
      zero teh config to garantee load a fresh one!
    */
    memset(&window_conf,0x0,sizeof(window_conf));
    window_config_load("wincfg.dat");
    return;
}

// after saves closes (use it when close )
void window_config_save(const char* filename)
{
    ALLEGRO_FILE *out = NULL;

    out = al_fopen(filename, "wb");

    al_fwrite32le(out, window_conf.width);
    al_fwrite32le(out, window_conf.height);
    al_fwrite32le(out, window_conf.fullscreen);
    al_fwrite32le(out, window_conf.opengl);
    al_fwrite32le(out, window_conf.use_mouse);
    al_fwrite(out, window_conf.window_name, 56 + 1);
    al_fwrite32le(out, window_conf.vsync);
    al_fclose(out);

}
void window_config_load(const char* filename){

    ALLEGRO_FILE *in = NULL;
    window_cfg *cfg = &window_conf;
    in = al_fopen(filename, "rb");
    al_fread(in,cfg, sizeof(window_conf));
    al_fclose(in);
}


int window_get_width(){
    return al_get_display_width(g_display);
}
int window_get_height(){
    return al_get_display_height(g_display);
}

char *window_get_title(){
    char *title;
    size_t size = 0;

    size = strnlen(window_conf.window_name, 56);
    title = malloc( sizeof(char) *  (size + 1));
    strcpy(title, window_conf.window_name);
    return title;
}

ALLEGRO_DISPLAY* window_get_display(){
    return g_display;
}

ALLEGRO_TIMER *window_get_timer_ingame(){
    return timer_ingame;
}

ALLEGRO_TIMER *window_get_timer(){
    return g_timer;
}

/*
    returns milliseconds of the timer
    this is meant to be used when you need something calculated in timer only
    for  high precision use the main timer updates every 1/60 ms

*/

int64_t window_get_milliseconds(void){
    return al_get_timer_count(timer_ingame) * 100;
}


void window_draw_to_screen(void){


        al_set_target_bitmap(al_get_backbuffer(g_display));

        int s_width, s_height;

        s_width  = al_get_bitmap_width(g_screen);
        s_height = al_get_bitmap_height(g_screen);

        al_draw_scaled_bitmap(g_screen, 0,0, s_width, s_height, 0,0, 640, 480,0);

        al_flip_display();

}

void window_draw_to_buffer(void){
    al_set_target_bitmap(g_screen);
}


void window_resize(int w, int h){
    al_acknowledge_resize(g_display);


    ALLEGRO_DISPLAY *buffer = al_create_display(w,h);
    ALLEGRO_DISPLAY *old_display = g_display;

    al_destroy_display(old_display);

    g_display = buffer;


    window_conf.width = w;
    window_conf.height = h;

    window_config_save("wincfg.dat");
    al_flip_display();


}

void window_reset_input(){
    al_flush_event_queue(g_queue);
}

ALLEGRO_BITMAP* window_get_screen(void){
    return g_screen;
}



void window_reset_display(){

    al_set_target_bitmap(al_get_backbuffer(g_display));

}



