#ifndef WINDOW_HEADER
#define WINDOW_HEADER

#include <stdio.h>
#include <allegro5/allegro5.h>
#include "../input/mouse.h"

#define COLOR_BLACK 0x000000

void window_init(void);
void window_close(void);
void window_config_init(void);
void window_config_save(const char* filename);
void window_config_load(const char* filename);
int window_get_width(void);
int window_get_height(void);
char *window_get_title(void);

ALLEGRO_BITMAP* window_get_screen(void);
void window_draw_to_screen(void);
void window_draw_to_buffer(void);
void window_resize(int w, int h);
void window_reset_input(void);


void init_phyfs(void);


extern int64_t *game_timer_count;
extern int64_t *game_timer_milliseconds;

ALLEGRO_DISPLAY* window_get_display(void);

ALLEGRO_TIMER *window_get_timer(void);
ALLEGRO_TIMER *window_get_timer_ingame(void);
int64_t window_get_milliseconds(void);

typedef struct window_config {
    int width;
    int height;
    int fullscreen;
    int opengl;
    int use_mouse;
    char window_name[56];
    int vsync;

}window_cfg;




extern ALLEGRO_EVENT_QUEUE *g_queue;
extern ALLEGRO_DISPLAY *g_display;
extern ALLEGRO_KEYBOARD_STATE g_kbdstate;
extern ALLEGRO_BITMAP *g_screen;
extern ALLEGRO_TIMER *g_timer;
extern ALLEGRO_TIMER *timer_ingame;
extern ALLEGRO_BITMAP *g_screen;

extern MouseInput mInput;
extern window_cfg window_conf;

void window_render_to_layer(int id);
void window_reset_display(void);
void window_render_layers(void);


#define FRAME_DURATION 0.016667
#define SET_TARGET(t) (al_set_target_bitmap((t)))
#define SCREEN_SET_TARGET(x) SET_TARGET(x)
#define RESET_DISPLAY() (al_set_target_backbuffer(window_get_display()))
#define SCREEN_RESET_DISPLAY() RESET_DISPLAY()

#define WINDOW_LAYER_BG 0
#define WINDOW_LAYER_LAYER0 1
#define WINDOW_LAYER_LAYER1 2
#define WINDOW_MAX_RENDER_LAYERS 3




#endif
