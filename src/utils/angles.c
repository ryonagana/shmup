#include "angles.h"
#include "math_helper.h"
#include "vector2.h"

double getAngle(float x, float y){

    return 1.5 * GAME_PI - atan2(y,x);
}



void angle_rotate_point(float cx, float cy, float angle, Vector2 *v){



    float xpos = (v->x * cos(angle)) - (v->y * sin(angle)) ;
    float ypos = (v->y * cos(angle)) + v->x * sin(angle);

    v->x = ypos;
    v->y = xpos;


}
