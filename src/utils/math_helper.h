#ifndef MATH_HELPER_HEADER
#include <stdio.h>
#include <math.h>
#include <allegro5/allegro5.h>




#define GAME_PI ALLEGRO_PI /* constant already normalized for allegro */


#define DEG2RAD(DEG) ((DEG)*((GAME_PI)/(180.0)))
#define RAD2DEG(RAD) ((RAD)*180/GAME_PI)


double calculate_distance(float a, float b);
double rand_f(double min, double max);

int rand_int(int max, int min);

#define RANDINT(max,min) rand_int((max),(min))

#endif
