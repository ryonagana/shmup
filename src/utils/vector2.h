#ifndef VECTOR2_HEADER
#define VECTOR2_HEADER

#include "vector2.h"

#include <stdio.h>
#include <stdint.h>

typedef struct _Vector2 {
    float x;
    float y;
} Vector2;



void  vector_Zero(Vector2 *v);
void  vector_Init(Vector2 *v, float x, float y);
void  vector_Set(Vector2 *dest, const Vector2 *orig);
void  vector_Add(Vector2 *dest, const Vector2 *orig);
void  vector_Sub(Vector2 *dest, const Vector2 *orig);
void  vector_Mul(Vector2 *dest, const Vector2 *orig);
void  vector_Div(Vector2 *dest, const Vector2 *orig);
float vector_Distance(const Vector2 *dest, const Vector2 *orig);
int   vector_Magnitude(const Vector2 *vec);
void  vector_Normalize(Vector2 *dest, const Vector2 *vec);
Vector2  vector_Reverse(const Vector2 *vec);
void vector_Copy(Vector2 *dest, const Vector2 *orig);
const Vector2* vector_Init2(float x, float y);
double vector_DotProduct(Vector2 *dest, const Vector2 *orig);
void vector_Reflection(Vector2 *dest,  Vector2 *incident, Vector2 *normal);
Vector2* vector_Rotation(const Vector2 *v, const Vector2 *w);
void vector_Free(void *v);

#endif
