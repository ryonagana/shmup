#include "game_std.h"




/*
 * http://cvsweb.openbsd.org/cgi-bin/cvsweb/src/lib/libc/string/strlcpy.c?rev=1.11
 * better use a solid implementation than my unsafe implementation
 */



/*
 *
  for some obscure way  all string has some random gargabe in it
  only the last one carries the '\0', to garantee all character to zero i just added a memset to reset
  the characters, it might not the "best" way to do it
*/


size_t g_strlcat(char *dst, const char *src, size_t siz)
{
        char *d = dst;
        const char *s = src;
        size_t n = siz;
        size_t dlen;

        /* Find the end of dst and adjust bytes left but don't go past end */
        while (n-- != 0 && *d != '\0')
                d++;
        dlen = d - dst;
        n = siz - dlen;

        if (n == 0)
                return(dlen + strlen(s));
        while (*s != '\0') {
                if (n != 1) {
                        *d++ = *s;
                        n--;
                }
                s++;
        }
        *d = '\0';

        return(dlen + (s - src));        /* count does not include NUL */
}


size_t g_strlcpy(char *dst, const char *src, size_t siz){
    char *d = dst;
    const char *s = src;
    size_t n = siz;

    /* Copy as many bytes as will fit */
    if (n != 0 && --n != 0) {
        do {
            if ((*d++ = *s++) == 0)
                break;
            } while (--n != 0);
        }

          /* Not enough room in dst, add NUL and traverse rest of src */
        if (n == 0) {
            if (siz != 0)
                *d = '\0';                /* NUL-terminate dst */
            while (*s++)
                          ;
          }
        return(s - src - 1);        /* count does not include NUL */
}
