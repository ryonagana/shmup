#ifndef UTILS_HEADER
#define UTILS_HEADER
#include <allegro5/allegro.h>

#ifndef FALSE
#define FALSE (0)
#endif

#ifndef TRUE
#define TRUE (!FALSE)
#endif




#if !defined(__APPLE__)
#define strlncpy(dest, orig, size) strncpy(dest,orig, size)
#endif

#ifdef INLINE_FUNCTION
#undef INLINE_FUNCTION
#endif

#if defined(WIN32) || defined(_WIN32) || defined(_MINGW32_)
    #define INLINE_FUNCTION __inline
#elif defined(__GNUC__) || defined(__APPLE__) || defined(__unix__)
    #define INLINE_FUNCTION inline
#else
    #define INLINE_FUNCTION
#endif


#define UNUSED_VAR(x) (void)(x)


void gracefully_quit(void);

ALLEGRO_COLOR Hex2RGB(int hexcode);
ALLEGRO_COLOR Hex2BGR(int hexcode);


#endif
