#ifndef ANGLES_HEADER
#define ANGLES_HEADER
#include <math.h>
#include "vector2.h"


typedef float matrix3x3[2][2];


double getAngle(float x, float y);
void angle_rotate_point(float cx, float cy, float angle, Vector2 *v);

#endif
