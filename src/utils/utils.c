#include "utils.h"
#include <stdio.h>
#include "../window/window.h"
#include "mixer.h"

void gracefully_quit(void){

    mixer_destroy();
    window_close();

    fprintf(stdout, "\n==== GRACEFULLY QUITTED ===\n");
}


ALLEGRO_COLOR Hex2RGB(int hexcode){
    unsigned char r,g,b;

    r = ((hexcode >> 16) & 0xFF);
    g = ((hexcode >> 8) & 0xFF);
    b = ((hexcode) & 0xFF);

    return al_map_rgb(r,g,b);


}
ALLEGRO_COLOR Hex2BGR(int hexcode){
    unsigned char r,g,b;

    b = ((hexcode >> 16) & 0xFF);
    g = ((hexcode >> 8) & 0xFF);
    r = ((hexcode) & 0xFF);

    return al_map_rgb_f(r,g,b);
}
