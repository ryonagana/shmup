#ifndef XALLOC_HEADER
#define XALLOC_HEADER

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>



void xfree(void** ptr);
void *xmalloc(size_t size, unsigned long line);
void* xcalloc (size_t num, size_t size, unsigned long line);
void* xrealloc (void* ptr, size_t size, unsigned long line);

#define safe_free(ptr) xfree( (void **) &(ptr))
#define safe_malloc(size) xmalloc(size,__LINE__)
#define safe_calloc(num,size) xcalloc(num,size,__LINE__)
#define safe_realloc(ptr,size) xrealloc(ptr,size,__LINE__)
#endif
