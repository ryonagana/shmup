#include "vector2.h"
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "utils.h"


void vector_Zero(Vector2 *v){
    v->x = 0;
    v->y = 0;

}


void vector_Init(Vector2 *v, float x, float y){
    v->x = x;
    v->y = y;
}

const Vector2* vector_Init2(float x, float y){
    Vector2 v;
    v.x = x;
    v.y = y;

    return &v;
}

void vector_Add(Vector2 *dest, const Vector2 *orig){
    if(dest != orig){
        dest->x += orig->x;
        dest->y += orig->y;
    }
}
void vector_Sub(Vector2 *dest, const Vector2 *orig){
    if(dest != orig){
        dest->x -= orig->x;
        dest->y -= orig->y;
    }
}
void vector_Mul(Vector2 *dest, const Vector2 *orig){
    if(dest != orig){
        dest->x *= orig->x;
        dest->y *= orig->y;
    }
}
void vector_Div(Vector2 *dest, const Vector2 *orig){
    if(dest != orig){
        dest->x /= orig->x;
        dest->y /= orig->y;
    }
}

float vector_Distance(const Vector2 *dest, const Vector2 *orig){
    float xdiff, ydiff, distance;

    xdiff = dest->x - orig->x;
    ydiff = dest->y - orig->y;

    distance = sqrt(( xdiff * xdiff  ) + (ydiff * ydiff));

    return distance;

}
int vector_Magnitude(const Vector2 *vec){
    return sqrt( (vec->x * vec->x) + (vec->y + vec->y) );
}
void vector_Normalize(Vector2 *dest, const Vector2 *vec){

    int mag = vector_Magnitude(vec);

    dest->x = vec->x / mag;
    dest->y = vec->y / mag;

}

void  vector_Set(Vector2 *dest, const Vector2 *orig){
   dest->x = orig->x;
   dest->y = orig->y;

}

Vector2  vector_Reverse(const Vector2 *vec){
    Vector2 nv;
    vector_Zero(&nv);
    vector_Init(&nv, -vec->x, -vec->y);
    return nv;
}

void vector_Copy(Vector2 *dest, const Vector2 *orig){
    if(dest == NULL){
        dest = malloc(sizeof(*orig));
    }

    dest->x = orig->x;
    dest->y = orig->y;

}



INLINE_FUNCTION double vector_DotProduct(Vector2 *dest, const Vector2 *orig ){
    return (dest->x * orig->x) + (dest->y * orig->y);
}


INLINE_FUNCTION void vector_Reflection(Vector2 *dest,  Vector2 *incident, Vector2 *normal){
    Vector2 tmp_incident;
    double dot;

    vector_Zero(&tmp_incident);

    dot = vector_DotProduct(normal, incident);

    tmp_incident.x = incident->x -= 2.f * dot;
    tmp_incident.y = incident->y -= 2.f * dot;

    vector_Mul(&tmp_incident, normal);
    vector_Set(dest, &tmp_incident);

}

Vector2 *vector_Rotation(const Vector2 *v, const Vector2 *w){
    Vector2 *res = NULL;

    res = malloc(sizeof(Vector2));

    res->x = (v->x * w->x) - (v->y * w->y);
    res->y = (v->x * w->y) - (v->y * w->x);

    return res;
}

void vector_Free(void *v){
    Vector2 *res = (Vector2*)v;
    if(res != NULL) free(res);
    return;
}

