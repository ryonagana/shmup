#ifndef COLLISION_HEADER
#define COLLISION_HEADER
#include <stdio.h>
#include "utils.h"


typedef struct GRect {
  float x, y;
  int w, h;
}GRect;


int collision_detect(GRect *a, GRect *b);
int aabb_collision(GRect *a, GRect *b);


#endif
