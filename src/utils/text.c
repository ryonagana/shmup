#include "text.h"
#include <stdarg.h>
#include "utils.h"
#include "../window/window.h"



void game_text_init(GameText *text, const char *font_path, int size, int animated){
    text->fnt = al_load_ttf_font(font_path, size, 0);

    if(text->fnt == NULL){
        text->fnt = al_create_builtin_font();
    }

    text->animated = animated;
    text->visible = TRUE;
    text->text = al_ustr_new("");
    text->w = 0;
    text->h = 0;
    text->speed = 0.0f;

}


void game_text_set_shadow_color_hex(GameText *text, int hex){
    text->shadow_color = Hex2RGB(hex);
}

void game_text_set_shadow_color(GameText *text, ALLEGRO_COLOR color){
    text->shadow_color = color;
}




void game_text_set_color_hex(GameText *text, int hex){
    text->color = Hex2RGB(hex);
}

void game_text_set_color(GameText *text, ALLEGRO_COLOR color){
    text->color = color;
}


void game_text_draw(GameText *text, float x, float y,  const char *fmt, ...){
    va_list list;
    char buffer[2048];

    va_start(list, fmt);
    vsprintf(buffer, fmt, list);
    va_end(list);

    text->text = al_ustr_new_from_buffer(buffer, strlen(buffer));

    text->w = al_get_ustr_width(text->fnt, text->text);

    al_draw_ustr(text->fnt, text->shadow_color, x + 1,y + 1,0, text->text);
    al_draw_ustr(text->fnt, text->color, x,y,0, text->text);


}


void game_text_draw_color(GameText *text, float x, float y, ALLEGRO_COLOR color, ALLEGRO_COLOR shadow,  const char *fmt, ...){
    va_list list;
    char buffer[2048];

    va_start(list, fmt);
    vsprintf(buffer, fmt, list);
    va_end(list);

    text->text = al_ustr_new_from_buffer(buffer, strlen(buffer));

    text->w = al_get_ustr_width(text->fnt, text->text);

    al_draw_ustr(text->fnt, shadow, x + 1,y + 1,0, text->text);
    al_draw_ustr(text->fnt, color, x,y,0, text->text);

}


void game_text_draw_ustr(GameText *text, float x, float y, ALLEGRO_COLOR color, ALLEGRO_COLOR shadow, const ALLEGRO_USTR *string_buffer) {

    al_draw_ustr(text->fnt, shadow, x + 1,y + 1,0, string_buffer);
    al_draw_ustr(text->fnt, color, x,y,0, string_buffer);

}


void game_text_destroy(GameText *text){
    if(text == NULL) return;

    if(text->fnt) al_destroy_font(text->fnt);
    if(al_ustr_length(text->text) > 0 || text->text != NULL) al_ustr_free(text->text);


    text->text = NULL;
    text->fnt = NULL;

}

void game_text_set_speed(GameText *text, float speed){
    text->speed = speed;
}
