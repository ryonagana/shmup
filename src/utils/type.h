#ifndef TYPES_HEADER
#define TYPES_HEADER
#include <stdint.h>
#include <stdbool.h>

typedef uint32_t       uint32;
typedef uint64_t       uint64;
typedef int            sint;
typedef unsigned       uint;
typedef _Bool          gbool;
typedef float          f32;
typedef double         f64;
typedef unsigned char  byte;
typedef char           sbyte;
#endif
