#include "debug.h"
#include <stdio.h>
#include <stdlib.h>
#include "utils.h"

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>

FILE *out_debug = NULL;
static ALLEGRO_FONT* default_font;

void debug_init(){
#ifdef DEBUG_LOG
    log_write("", "== LOG INITIALIZED ==");
    default_font = al_create_builtin_font();

#endif

}

void debug_destroy(){
    if(out_debug != NULL){
        fclose(out_debug);
        out_debug = NULL;
    }
}

INLINE_FUNCTION void log_write(const char *type, const char *msg, ...){


    char buf[255];
    va_list vl;
    va_start(vl, msg);
    vsprintf(buf, msg, vl);
    out_debug = fopen("game.log", "a");
    fprintf(out_debug, "%s: %s\n", type, buf);
    fprintf(stdout, "%s: %s\n", type, buf);
    fclose(out_debug);
    va_end(vl);


}

void debug_args_message(const char *fmt, ...){

    do {
        va_list list;
        char buffer[255];

        va_start(list, fmt);
        vsprintf(buffer, fmt, list);

        fprintf(stdout, "%s\n", buffer);

        va_end(list);
    }while(0);

}


void print_text(float x, float y, const char *fmt, ...){
 #ifdef DEBUG_LOG
        char buf[1024];
        va_list list;

        va_start(list, fmt);

        vsnprintf(buf, sizeof buf, fmt, list);
        va_end(list);
        al_draw_text(default_font, al_map_rgb(255,0,0), x,y, 0, buf);
        return;
 #endif


    return;
}




