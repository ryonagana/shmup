#include "resources.h"



static void resource_change_directory(const char *directory){
     ALLEGRO_PATH *p;
     p = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
     al_append_path_component(p, "assets");
     if(directory){
        al_append_path_component(p,directory);
     }
     al_change_directory(al_path_cstr(p,ALLEGRO_NATIVE_PATH_SEP));
     al_destroy_path(p);
}

ALLEGRO_BITMAP* resource_load_sprite(const char* str){
    ALLEGRO_BITMAP *res = NULL;

    resource_change_directory("sprites");

    res = al_load_bitmap(str);
    return res;

}

ALLEGRO_SAMPLE* resource_load_sound(const char *str){
    ALLEGRO_SAMPLE *snd = NULL;
    resource_change_directory("sfx");
    snd = al_load_sample(str);
    return snd;
}

ALLEGRO_FONT *resource_load_ttf(const char *path, int size, int flags){
    ALLEGRO_FONT *fnt = NULL;
    resource_change_directory("fonts");
    fnt = al_load_ttf_font(path, size,flags);
    return fnt;
}
