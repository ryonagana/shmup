#ifndef GAME_STD_HEADER
#define GAME_STD_HEADER
#include <stdio.h>
#include <string.h>

#if defined(__linux) || defined(__unix)
#include <sys/types.h>
#endif



size_t g_strlcpy(char *dst, const char *src, size_t siz);
size_t g_strlcat(char *dst, const char *src, size_t siz);



#if !defined(__APPLE__) || !defined(__FreeBSD__)
    #define strlcpy(dst, src, siz) g_strlcpy(dst,src,siz)
    #define strlcat(dst, src, siz) g_strlcat(dst,src,siz)
#else
    #undef strlcpy
    #undef strlcat
#endif



#endif
