#ifndef DEBUG_HEADER
#define DEBUG_HEADER

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>


void debug_init(void);
void debug_destroy(void);
void debug_args_message(const char *fmt, ...);
void log_write(const char *type, const char *msg, ...);
void print_console(float x, float y, const char *fmt, ...);

#define DEBUG_LOG_FILENAME "game.log"



#define XASSERT(x) do { \
					assert(x);\
					}while(0);\


#ifdef __GNUC__
#define DPRINT(x,...) debug_args_message(x, __VA_ARGS__)
#elif __MINGW32__
#define DPRINT(x,args...) debug_args_message(x,args)
#else
	#define DPRINT(x,...)
#endif




#ifdef DEBUG_LOG


extern FILE *out_debug;



#ifdef __GNUC__

#if __MINGW32__

#undef W2LOG
#undef  W2LOG_WARNING
#undef  W2LOG_CRITICAL

#define  W2LOG(m,...) log_write("LOG: ", m, __VA_ARGS__)
#define  W2LOG_WARNING(m,...) log_write("WARNING: ", m, __VA_ARGS__)
#define  W2LOG_CRITICAL(m,...) log_write("CRITICAL: ", m, __VA_ARGS__)

#endif

#undef  W2LOG
#undef  W2LOG_WARNING
#undef  W2LOG_CRITICAL

#define  W2LOG(m,...) log_write("LOG: ", m, ##__VA_ARGS__)
#define  W2LOG_WARNING(m,...) log_write("WARNING: ", m, ##__VA_ARGS__)
#define  W2LOG_CRITICAL(m,...) log_write("CRITICAL: ", m, ##__VA_ARGS__)




#else

/* TODO __VA_ARGS__ COMPATIBLE WITH AT LEAST WINBL0WZ */

#define W2LOG(m,...)
#define W2LOG_WARNING(m,...)
#define W2LOG_CRITICAL(m,...)
#endif


#endif

#ifndef DEBUG_LOG
#define W2LOG(m,...)
#define W2LOG_WARNING(m,...)
#define W2LOG_CRITICAL(m,...)
#endif

void print_text(float x, float y, const char *fmt, ...);

#endif // DEBUG_HEADER
