#ifndef TEXT_HEADER
#define TEXT_HEADER

#include <allegro5/allegro5.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include "vector2.h"

typedef struct GameText {
    ALLEGRO_COLOR color;
    ALLEGRO_COLOR shadow_color;
    ALLEGRO_USTR *text;
    ALLEGRO_FONT *fnt;
    float x,y;
    int w, h;
    int visible;
    float speed;
    int animated;
}GameText;

void game_text_init(GameText *text, const char *font_path, int size, int animated);
void game_text_draw(GameText *text, float x, float y,  const char *fmt, ...);
void game_text_set_color(GameText *text, ALLEGRO_COLOR color);
void game_text_set_color_hex(GameText *text, int hex);
void game_text_set_speed(GameText *text, float speed);
void game_text_set_shadow_color_hex(GameText *text, int hex);
void game_text_set_shadow_color(GameText *text, ALLEGRO_COLOR color);
void game_text_draw_color(GameText *text, float x, float y, ALLEGRO_COLOR color, ALLEGRO_COLOR shadow,  const char *fmt, ...);
void game_text_draw_ustr(GameText *text, float x, float y, ALLEGRO_COLOR color, ALLEGRO_COLOR shadow, const ALLEGRO_USTR *string_buffer);
void game_text_destroy(GameText *text);
#endif
