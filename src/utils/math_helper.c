#include "math_helper.h"
#include <math.h>
#include "utils.h"

INLINE_FUNCTION double calculate_distance(float a, float b){
    return sqrt( (a*a) + (b*b));
}

INLINE_FUNCTION double rand_f(double min, double max){
    float range = min - max;
    int n = rand() % 1000;
    float f = range * n / 1000.0f;
    return min+f;
}

INLINE_FUNCTION int rand_int(int max, int min){
    return rand() % ( (max + 1) - min ) + min;
}
