#include "xalloc.h"
#include <stdarg.h>

static void safe_call_message(unsigned long line, const char *fmt, ...){
    char buf[255+1];
    va_list list;

    va_start(list,fmt);
    vsnprintf(buf, 255, fmt, list);
    va_end(list);

    fprintf(stderr, "%s", buf);
}


void* xmalloc(size_t size, unsigned long line){
    void *block = NULL;

    block = malloc(size);

    if(!block){
        safe_call_message(line, "Out of Memory");
        return NULL;
    }

    return block;
}

void xfree(void **ptr){
    if(ptr != NULL || *ptr != NULL){
        free(*ptr);
        *ptr = NULL;
    }else {
        safe_call_message(__LINE__, "Mem free Error");
    }
}


void* xcalloc (size_t num, size_t size, unsigned long line){
    void *block = NULL;

    block = calloc(num,size);

    if(!block){
        safe_call_message(line, "Calloc Failed");
        return NULL;
    }

    return block;
}


void* xrealloc (void* ptr, size_t size, unsigned long line){
    void *tmp_block = NULL;

    tmp_block = realloc(ptr, size);

    if(tmp_block == NULL){
        safe_call_message(line, "Realloc Failed!");
        return NULL;
    }

    return tmp_block;

}
