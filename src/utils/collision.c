#include "collision.h"



int collision_detect(GRect *a, GRect *b){
    int Amin, Amax, Bmin, Bmax;

    Amin = a->x;
    Amax = Amin + a->w;
    Bmin = b->x;
    Bmax = Bmin + b->w;

    if(Bmin > Amin){
        Amin = Bmin;
    }

    if(Bmax < Amax){
        Amax = Bmax;
    }

    if(Amax <= Amin){
        return FALSE;
    }

    Amin = a->y;
    Amax = Amin + a->h;
    Bmin = b->y;
    Bmax = Bmin + b->h;

    if(Bmin > Amin){
        Amin = Bmin;
    }

    if(Bmax < Amax){
        Amax = Bmax;
    }

    if(Amax <= Amin){
        return FALSE;
    }


    return TRUE;




}


int aabb_collision(GRect *a, GRect *b){

    if(a->x < (b->x + b->w) &&
       (a->x + a->w) > b->x &&
       a->y < (b->y + b->h) &&
       (a->y + a->h) > b->y){
        return TRUE;
     }
    return FALSE;
}


