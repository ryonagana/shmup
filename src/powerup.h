#ifndef  POWERUP_HEADER
#define POWERUP_HEADER

#include <stdio.h>
#include <stdlib.h>

#include "utils/utils.h"
#include "player.h"
#include "graphic/sprite.h"
#include "window/window.h"
#include "utils/utils.h"
#include "utils/text.h"
#include "shot.h"

#define MAX_POWERUPS_SCREEN 8



#define POWERUP_STATE_STOPPED         0
#define POWERUP_STATE_MOVING          1
#define POWERUP_STATE_BOUNCING        2




#define POWERUP_DIR_INVERSE 0
#define POWERUP_DIR_REVERSE 1

typedef enum PowerupId {
    POWERUP_DOUBLEDMG = 0,
    POWERUP_DOUBLEWEAP,
    POWERUP_SHIELD,
    POWERUP_INVINCIBILITY,
    POWERUP_LAST_UNUSED
}PowerupId;


typedef struct Powerup {
    PowerupId id;
    Vector2 pos;
    Vector2 accel;
    int alive;
    float angle;
    int64_t time;
    float alpha;
    int64_t counter;
    int bounces;
    int type;
    int state;
    int w;
    int h;
    void (*Think)(struct Powerup *this, Player *player);

}Powerup;


extern Powerup  powerupList[MAX_POWERUPS_SCREEN];

void powerup_init(void);
void powerup_add(PowerupId id, const Vector2 *position, const Vector2 *accel, int bounces, int time, int counter, void (*ThinkCallback)(Powerup *powerup, Player *player), sprite *sprite);
void powerup_put(PowerupId id,  float x, float y, float xv, float yv);
void powerup_update(Player *p);
void powerup_render(void);
void powerup_kill(Powerup *p);
int powerup_collision(Powerup *powerup, Player *thePlayer);
int powerup_shot_collision(Powerup *powerup, Shot *theShot);



void powerup_pickup(GameText *text, Powerup *powerup, PowerupId id, int time, Vector2 *pos );


typedef struct Powerup_Modifer {
    int id;
    int counter;
    int64_t timer;

}Powerup_Modifier;

extern Powerup_Modifier powerupmodList[POWERUP_LAST_UNUSED + 1];

#endif
