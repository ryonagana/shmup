#include "editor.h"
#include "window/scene.h"
#include "window/window.h"
#include "utils/utils.h"

#define GRID_SIZE 32


static void Editor_Init(){

}

static void Editor_Start(){

}

static void Editor_Update(ALLEGRO_EVENT *e){
    UNUSED_VAR(e);
}


static void Editor_Render(){
    int i;

    for(i = 0; i < window_get_width() / GRID_SIZE; i++ ){
        al_draw_rectangle(i * GRID_SIZE, window_get_height(), i * GRID_SIZE, 0, Hex2RGB(0xFF0000), 1.0);
        al_draw_rectangle(window_get_width(), i * GRID_SIZE, 0, i * GRID_SIZE, Hex2RGB(0xFF0000), 1.0 );
    }

}


static void Editor_Update_Keyboard(ALLEGRO_EVENT *e){

}

static void Editor_Destroy(){

}




Scene editorScene = {
    &Editor_Init,
    &Editor_Start,
    &Editor_Update,
    &Editor_Render,
    &Editor_Update_Keyboard,
    &Editor_Destroy,
    0,
    0,
    FALSE
};
