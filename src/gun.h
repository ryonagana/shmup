#ifndef WEAPON_HEADER
#define WEAPON_HEADER
#include "utils/utils.h"
#include "utils/vector2.h"

#define MAX_WEAPONS (8)




typedef struct WeaponConfig {
    int id;
    int64_t extra_counter;
    int64_t counter;
    int64_t timer;
    int dmg;
    Vector2 accel;
    int sfx;

}WeaponConfig;

typedef enum ShipGuns {
    WEAPON_DEF_WCANNON = 0,
    WEAPON_WLASER
}ShipGuns;



extern WeaponConfig weapon_config[MAX_WEAPONS];



void gun_init(void);
void gun_set(int index, int id, int64_t timer, int dmg, const Vector2 *accel, int sfx);

#endif
