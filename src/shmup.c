#include <stdio.h>
#include <time.h>

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>
#include "window/window.h"
#include "window/scene.h"
#include "starfield/starfield.h"
#include "player.h"
#include "ui/ui.h"
#include "graphic/sprite.h"
#include "graphic/fade.h"
#include "enemy.h"
#include "utils/utils.h"
#include "sound_res.h"
#include <physfs.h>


extern ALLEGRO_EVENT_QUEUE *g_queue;
extern ALLEGRO_DISPLAY *g_display;
extern ALLEGRO_KEYBOARD_STATE g_kbdstate;
extern ALLEGRO_BITMAP *g_screen;
extern ALLEGRO_TIMER *g_timer;
extern ALLEGRO_TIMER *timer_ingame;


int loopClose = FALSE;


extern Scene mainGame;
extern Scene sGameOver;
extern Scene editorScene;


static int handlingResize = FALSE;

int main(int argc, char *argv[]){

    (void) argc;
    (void) argv;

    srand((unsigned int)time(NULL));

    PHYSFS_init(argv[0]);
    init_phyfs();

    window_init();
    mixer_init();
    sound_resource_init();
    scene_start();

#if DEBUG_LOG
    debug_init();
#endif


    scene_insert(&mainGame,    SCENE_MAINGAME);
    scene_insert(&sGameOver,   SCENE_GAMEOVER);
    scene_insert(&editorScene, SCENE_EDITOR);





    scene_init();
    scene_load(SCENE_MAINGAME);


    window_config_init();


    while(!loopClose){
        ALLEGRO_EVENT e;

            al_wait_for_event(g_queue, &e);



            if(e.type == ALLEGRO_EVENT_DISPLAY_CLOSE){
                loopClose = TRUE;
            }

            if(e.type == ALLEGRO_EVENT_TIMER) {

                if(e.timer.source == g_timer){

                    *(game_timer_count) = e.timer.count;

                    scene_update_game(&e);
                }

                if(e.timer.source == timer_ingame){
                    *(game_timer_milliseconds) = e.timer.count * 100;
                }
            }

            scene_update_keyboard(&e);




            if(e.type == ALLEGRO_EVENT_DISPLAY_RESIZE){
                handlingResize = TRUE;
            }else {
                handlingResize = FALSE;
            }


        if(al_is_event_queue_empty(g_queue)){

            al_clear_to_color(Hex2RGB(COLOR_BLACK));
            scene_render();
            al_flip_display();

        }

    }

    scene_destroy();
    window_config_save("wincfg.dat");

#if DEBUG_LOG
    debug_destroy();
#endif
    PHYSFS_deinit();
    atexit(&gracefully_quit);



    return 0;

}

