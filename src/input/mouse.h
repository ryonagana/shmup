#ifndef MOUSE_HEADER
#define MOUSE_HEADER
#include <allegro5/allegro5.h>

typedef struct MouseInput {
    float x;
    float y;
    int rButton;
    int lButton;
    float mWheelUp;
    float mWheelDown;
}MouseInput;

void mouse_init(MouseInput *mInput);
void mouse_update_coord(ALLEGRO_EVENT *e);
void mouse_update_buttons(ALLEGRO_EVENT *e);
#endif
