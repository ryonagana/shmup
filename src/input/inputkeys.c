#include "inputkeys.h"
#include "../window/window.h"
#include "../utils/utils.h"

extern ALLEGRO_EVENT_QUEUE *g_queue; /* ../window.c */

void inputkeys_init(textData *text, const char *ttf_path, int size){
    text->inputEnabled = TRUE;
    text->utext = al_ustr_new("");
    text->x = 0;
    text->y = 0;
    text->count = 0;
    text->confirmed = FALSE;

    text->font = text->font == NULL ?  al_create_builtin_font() : al_load_ttf_font(ttf_path, size, ALLEGRO_TTF_NO_AUTOHINT);
    text->shadow = text->shadow == NULL ?  al_create_builtin_font() : al_load_ttf_font(ttf_path, size, ALLEGRO_TTF_NO_AUTOHINT);
}


void inputkeys_update(ALLEGRO_EVENT *e, textData *text){
    int unichar;
    if(!text->inputEnabled) return;

         unichar = e->keyboard.unichar;

        if( e->keyboard.unichar >= 32){
         text->count += al_ustr_append_chr(text->utext, unichar);
        }else if( e->keyboard.keycode == ALLEGRO_KEY_BACKSPACE){

            if(al_ustr_prev(text->utext, &(text->count))) al_ustr_truncate(text->utext,text->count);

        }else if(e->keyboard.keycode == ALLEGRO_KEY_ENTER ||
                 e->keyboard.keycode == ALLEGRO_KEY_PAD_ENTER){
                 text->confirmed = TRUE;
                 text->inputEnabled = FALSE;
        }

}

void inputkeys_draw(textData *text, ALLEGRO_COLOR color){
    al_draw_ustr(text->font, color, text->x, text->y, 0, text->utext);
}

void inputkeys_draw_shadow(textData *text, ALLEGRO_COLOR color, ALLEGRO_COLOR shadow_color){
    al_draw_ustr(text->shadow, shadow_color, text->x - 2, text->y - 2, 0, text->utext);
    al_draw_ustr(text->font, color, text->x, text->y, 0, text->utext);
}

void inputkeys_set_pos(textData *t, float x, float y){
    t->x = x;
    t->y = y;
}

void inputkeys_destroy(textData *t){
    if(t->utext) al_ustr_free(t->utext);
    if(t->font) al_destroy_font(t->font);
    if(t->shadow) al_destroy_font(t->shadow);
}
