#ifndef INPUTKEYS_HEADER
#define INPUTKEYS_HEADER

#include <allegro5/allegro5.h>
#include <allegro5/allegro_ttf.h>

#define MAX_INPUT_TEXT_BUF 255

typedef struct  _textData {
    ALLEGRO_USTR *utext;
    int inputEnabled;
    int confirmed;
    float x;
    float y;
    int count;
    ALLEGRO_FONT *font;
    ALLEGRO_FONT *shadow;
}textData;

void inputkeys_init(textData *text, const char *ttf_path, int size);
void inputkeys_destroy(textData *t);
void inputkeys_update(ALLEGRO_EVENT *e, textData *text);
void inputkeys_draw(textData *text, ALLEGRO_COLOR color);
void inputkeys_draw_shadow(textData *text, ALLEGRO_COLOR color, ALLEGRO_COLOR shadow_color);
void inputkeys_set_pos(textData *t, float x, float y);
#endif
