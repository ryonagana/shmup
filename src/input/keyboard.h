#ifndef KEYBOARD_HEADER
#define KEYBOARD_HEADER

#include <allegro5/allegro5.h>

extern uint32_t keys[ALLEGRO_KEY_MAX];

void keyboard_init(void);

int keyboard_keypressed (int key);
int keyboard_keyreleased(int key);
#endif
