#include "mouse.h"
#include "../window/window.h"
#include "../utils/utils.h"

void mouse_init(MouseInput *mInput){
    mInput->lButton = 0x0;
    mInput->rButton = 0x0;
    mInput->mWheelDown = 0.0;
    mInput->mWheelUp   = 0.0;
    mInput->x = 0;
    mInput->y = 0;

    return;
}


void mouse_update_coord(ALLEGRO_EVENT *e){

    if(e->type == ALLEGRO_EVENT_MOUSE_AXES || e->type == ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY){
        mInput.x = e->mouse.x;
        mInput.y = e->mouse.y;
    }

}

void mouse_update_buttons(ALLEGRO_EVENT *e){

    if(e->type == ALLEGRO_EVENT_MOUSE_BUTTON_UP){
        if(e->mouse.button & 1){
            mInput.lButton = 0x1;
        }else {
            mInput.lButton = 0x0;
        }

        if(e->mouse.button & 2){
            mInput.rButton = 0x1;
        }else {
            mInput.rButton = 0x0;
        }
    }

}




int player_mouse_button(ALLEGRO_EVENT *e, int button){
    return  e->mouse.button & button;
}
