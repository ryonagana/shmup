#include "keyboard.h"
#include "../utils/utils.h"

uint32_t keys[ALLEGRO_KEY_MAX];

void keyboard_init(){
    int i;
    for(i = 0; i < ALLEGRO_KEY_MAX; i++ ){
        keys[i] = FALSE;
    }
}

int keyboard_keypressed (int key){
    return (int)keys[key];
}
int keyboard_keyreleased(int key){
    return !(int) keys[key];
}
