#ifndef FADE_HEADER
#define FADE_HEADER

#include <stdio.h>
#include <allegro5/allegro5.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>

enum fade_type {
    FADE_IN = 0,
    FADE_OUT,
    FADE_UNUSED
};

typedef struct  {
    int64_t counter;
    float alpha;
    enum fade_type type;
}fade_t;


void fade_init(void);
void fade_in(int time);
void fade_out(int time);
void fade_update(void);
void fade_render(void);
void fade_destroy(void);
#endif
