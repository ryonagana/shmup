#ifndef PARTICLES_HEADER
#define PARTICLES_HEADER
#include "../utils/vector2.h"
#include <allegro5/allegro5.h>
#include "sprite.h"

typedef struct Particle {
    ALLEGRO_COLOR color;
    float size;
    float x,y;
    float vx,vy; /* velocity */
    float age,mass;
    int alive;

    struct Particle* next;

}Particle;



typedef struct ParticleTree {
    Particle *root;
    Particle *head;
    int numParticles;
    int type;
    int is_paused;
    void (*ParticleBehaviour)(Particle *theParticle);
}ParticleTree;


#define PARTICLE_MAX_VEL 0.999
#define PARTICLE_MIN_VEL 0.0

#define PARTICLE_MAX_VALUE (10)

#define PARTICLE_TYPE_COLORPIXEL2X2 0


void particles_update(ParticleTree *particles);


#endif
