#include "effects.h"
#include "../window/window.h"
#include "../utils/math_helper.h"
#include "../utils/utils.h"
#include "../starfield/starfield.h"
#include "../utils/debug.h"

static int shakeScreenCount = 0;
static int shakeCount = 0;
static float bgx = 0;
static float bgy = 0;


struct ready_t {
    int counter;
    int state;
    int timer;
    float alpha;
    float x,y;
    int active;
    int w,h;
};

struct ready_t ready_data = {
    0,
    READY_STATE_NONE,
    0,
    1.0,
    0.0,
    0.0,
    FALSE,
    30,
    30
};






extern ALLEGRO_BITMAP *ready_img;
extern ALLEGRO_BITMAP *go_img;



void shake_screen(void){

    if(shakeScreenCount <= 0){
        shakeScreenCount = SHAKE_TIME;

        bgx = rand_f(0.0, 8.0);
        bgy = rand_f(0.0, 8.0);
        if( RANDINT(0,2) % 2 ) bgx = -bgx;
        if( RANDINT(0,2) % 2 ) bgy = -bgy;
    }

}


void shake_screen_update(int step){
    if(shakeScreenCount > 0){
        shakeScreenCount -= step;
        DPRINT("%d", shakeScreenCount);
        if(shakeScreenCount <= 0){
            shakeCount++;

            if(shakeCount >= SHAKE_TIME){

                shakeCount = 0;
                shakeScreenCount = 0;
                bgx = 0;
                bgy = 0;

            }else{
                bgx = rand_f(0.0, 8.0);
                bgy = rand_f(0.0, 8.0);
                if( RANDINT(0,16) % 2 ) bgx = -bgx;
                if( RANDINT(0,16) % 2 ) bgy = -bgy;
                shakeScreenCount = SHAKE_TIME;
            }

        }

    }
}


void shake_screen_render(void){
    starfield_draw(bgx,bgy);
}

int  shake_get_counter(){
    return shakeScreenCount;
}


void ready_text(void){

    if(ready_data.state == READY_STATE_NONE){
         ready_data.timer = *game_timer_milliseconds + READY_TIME;
         ready_data.alpha = 1.0;
         ready_data.active = TRUE;
         ready_data.state = READY_STATE_RDY;
         ready_data.x = 0;
         ready_data.y = (window_get_height() / 2) + al_get_bitmap_width(ready_img);
         ready_data.w = 0;
         ready_data.h = 0;
    }


}

int ready_update(void){

    if(ready_data.timer > 0){
        ready_data.counter = *game_timer_milliseconds;

        if(ready_data.counter >= ready_data.timer && ready_data.state == READY_STATE_RDY){
            ready_data.timer =  *game_timer_milliseconds + 900;
            ready_data.counter = 0;
            ready_data.state = READY_STATE_GO;
            ready_data.y = (window_get_height() / 2) + al_get_bitmap_width(ready_img);
            ready_data.x = 0;
        }else {
            ready_data.x += 6.0;

            if(ready_data.alpha < 1.0 ){
                ready_data.alpha += 0.1;
            }
        }


        if(ready_data.counter >= ready_data.timer && ready_data.state == READY_STATE_GO){
            ready_data.timer =  0;
            ready_data.counter = 0;
            ready_data.state = READY_STATE_END;
            ready_data.active = FALSE;

        }else {

            int w = al_get_bitmap_width(go_img);
            int h = al_get_bitmap_height(go_img);


            if(ready_data.w <= w ){
                ready_data.w += 1;
            }

            if(ready_data.h <= h){
                ready_data.h += 1;
            }


        }

    }

    return ready_data.state;
}


void ready_render(void){



    DPRINT("update time: %d -> timer: %d -> future %d -> active: %s", ready_data.counter, ready_data.timer, 0, ready_data.active ? "ACTIVE" : "NO ACTIVE");


    al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);
    if(ready_data.state == READY_STATE_RDY){
        al_draw_tinted_bitmap(ready_img, al_map_rgba_f(1.0,1.0,1.0, ready_data.alpha), ready_data.x,ready_data.y,0);

    }

    if(ready_data.state == READY_STATE_GO){



        int w = al_get_bitmap_width(go_img);
        int h = al_get_bitmap_height(go_img);

        al_draw_tinted_scaled_bitmap(go_img,
                                     al_map_rgba_f(1.0,1.0,1.0, ready_data.alpha),
                                     0,0,
                                     w,h,
                                     (window_get_width() / 2) - w,
                                     (window_get_height() / 2) - h,
                                     ready_data.w,ready_data.h,
                                     0);


    }

    if(ready_data.state == READY_STATE_END){
        ready_data.state = READY_STATE_NONE;
    }




}
