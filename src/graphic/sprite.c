#include "sprite.h"
#include "../utils/resources.h"
#include "../window/window.h"
#include "../utils/utils.h"
#include "../utils/json.h"
#include <stdio.h>
#include <stdlib.h>

extern int64_t *game_timer_milliseconds; // window.c

void sprite_init_c(sprite* sprite, int animations_initial_capacity) {
    sprite->animations = malloc(sizeof(sprite_animation) * animations_initial_capacity);
    if (sprite->animations == NULL)
        exit (EXIT_FAILURE);

    sprite->animations_capacity = animations_initial_capacity;
    sprite->animations_count = 0;
    sprite->current_animation = NULL;
    sprite->current_frame_index = 0;
    sprite->before_destroy = NULL;
    sprite->flags.paused |= FALSE;
}

void sprite_init(sprite* sprite) {
    sprite_init_c(sprite, SPRITE_DEFAULT_ANIMATIONS_CAPACITY);
}

void sprite_destroy(sprite* sprite) {
    if (sprite->before_destroy != NULL)
        sprite->before_destroy(sprite);

    for (int i = 0; i < sprite->animations_count; i++) {
        sprite_animation_destroy(&sprite->animations[i]);
    }

    free(sprite->animations);
}

sprite_animation* sprite_add_animation(sprite* sprite, sprite_animation animation) {
    if (sprite->animations_count + 1 > sprite->animations_capacity) {
        if (sprite->animations_capacity <= 0) sprite->animations_capacity = 1;
        else sprite->animations_capacity *= 2;
        sprite->animations = realloc(sprite->animations, sizeof(sprite_animation) * sprite->animations_capacity);

        if (sprite->animations == NULL)
            exit (EXIT_FAILURE);
    }

    sprite_animation* animation_address = &sprite->animations[sprite->animations_count];
    *animation_address = animation;

    if (sprite->current_animation == NULL) {
        sprite->current_animation = animation_address;
    }

    sprite->animations_count += 1;
    return animation_address;
}

void sprite_animation_init_c(sprite_animation *animation, const char* name, int frames_initial_capacity) {
    animation->frames = malloc(sizeof(sprite_animation_frame) * frames_initial_capacity);
    if (animation->frames == NULL)
        exit (EXIT_FAILURE);

    animation->frames_capacity = frames_initial_capacity;
    animation->frames_count = 0;
    animation->repeat_at_frame_index = 0;
    animation->flags.repeat = TRUE;
    g_strlcpy(animation->name, name, ANIMATION_NAME_MAX_LENGTH - 1);
}

void sprite_animation_init(sprite_animation *animation, const char* name) {
    sprite_animation_init_c(animation, name, SPRITE_ANIMATION_DEFAULT_FRAMES_CAPACITY);
}

void sprite_animation_destroy(sprite_animation* sprite) {
    free(sprite->frames);
}

sprite_animation_frame* sprite_animation_add_frame(sprite_animation* animation, sprite_animation_frame frame) {
    if (animation->frames_count + 1 > animation->frames_capacity) {
        if (animation->frames_capacity <= 0) animation->frames_capacity = 1;
        else animation->frames_capacity *= 2;
        animation->frames = realloc(animation->frames, sizeof(sprite_animation_frame) * animation->frames_capacity);

        if (animation->frames == NULL)
            exit (EXIT_FAILURE);
    }

    sprite_animation_frame* frame_address = &animation->frames[animation->frames_count];

    *frame_address = frame;
    animation->frames_count += 1;

    return frame_address;
}

void sprite_play_animation(sprite* sprite, const char* animation_name) {
    if (strncmp(sprite->current_animation->name, animation_name, ANIMATION_NAME_MAX_LENGTH) == 0) {
        // Animation already playing!
        return;
    }

    int animation_index = -1;
    for (int i = 0; i < sprite->animations_count; i++) {
        if (strncmp(sprite->animations[i].name, animation_name, ANIMATION_NAME_MAX_LENGTH) == 0) {
            // Animation found
            animation_index = i;
            break;
        }
    }

    if (animation_index < 0) {
        // Animation not found!
        return;
    }

    // Starting animation
    sprite->current_animation = &sprite->animations[animation_index];
    sprite->current_frame_elapsed_time = 0;
    sprite->current_frame_index = 0;
}

void sprite_set_paused(sprite* sprite, int paused) {
    sprite->flags.paused = paused;
}

int sprite_get_width(sprite* sprite) {
    if (sprite->current_animation == NULL)
        return 0;
    return sprite->current_animation->frames[sprite->current_frame_index].width;
}

int sprite_get_height(sprite* sprite) {
    if (sprite->current_animation == NULL)
        return 0;
    return sprite->current_animation->frames[sprite->current_frame_index].height;
}

// void sprite_animation_completed(sprite* sprite) {
// }

void sprite_update(sprite* sprite, ALLEGRO_EVENT *e) {

    UNUSED_VAR(e);

    if (sprite->flags.paused || sprite->current_animation == NULL) {
        return;
    }

    sprite->current_frame_elapsed_time += FRAME_DURATION;

    if (sprite->current_frame_elapsed_time >= sprite->current_animation->frames[sprite->current_frame_index].duration) {
        if (sprite->current_frame_index + 1 >= sprite->current_animation->frames_count && !sprite->current_animation->flags.repeat) {
            //sprite_animation_completed(sprite);
            return;
        }

        sprite->current_frame_index += 1;

        if (sprite->current_frame_index >= sprite->current_animation->frames_count) {
            sprite->current_frame_index = sprite->current_animation->repeat_at_frame_index;
        }

        sprite->current_frame_elapsed_time = 0;
    }
}

void sprite_draw(sprite* sprite, float x, float y) {
    if (sprite->current_animation == NULL) {
        return;
    }

    sprite_animation_frame* current_frame = &sprite->current_animation->frames[sprite->current_frame_index];
     al_draw_bitmap_region(
         current_frame->bitmap,
         current_frame->x,
         current_frame->y,
         current_frame->width,
         current_frame->height,
         x - current_frame->anchor_x * current_frame->width,
         y - current_frame->anchor_y * current_frame->height,
         0
    );
}


void sprite_draw_tinted(sprite* sprite, float x, float y, ALLEGRO_COLOR color) {
    if (sprite->current_animation == NULL) {
        return;
    }

    sprite_animation_frame* current_frame = &sprite->current_animation->frames[sprite->current_frame_index];

    al_draw_tinted_bitmap_region(
        current_frame->bitmap,
        color,
        current_frame->x,
        current_frame->y,
        current_frame->width,
        current_frame->height,
        x - current_frame->anchor_x * current_frame->width,
        y - current_frame->anchor_y * current_frame->height,
        0
   );


}

void sprite_draw_rotate(sprite* sprite, float pivot_x, float pivot_y, float x, float y, float angle){
    if (sprite->current_animation == NULL) {
        return;
    }

    sprite_animation_frame* current_frame = &sprite->current_animation->frames[sprite->current_frame_index];

     al_draw_rotated_bitmap(current_frame->bitmap, pivot_x, pivot_y, x,y, angle, 0);

}

void sprite_draw_rotate_tinted(sprite* sprite, ALLEGRO_COLOR tint, float pivot_x, float pivot_y, float x, float y, float angle){
    if (sprite->current_animation == NULL) {
        return;
    }

    sprite_animation_frame* current_frame = &sprite->current_animation->frames[sprite->current_frame_index];

    al_draw_tinted_rotated_bitmap(current_frame->bitmap, tint, pivot_x, pivot_y, x,y, angle,0);


}




// Common

ALLEGRO_BITMAP* safe_load_image(const char* bitmap_file_path) {
    ALLEGRO_BITMAP* bitmap = resource_load_sprite(bitmap_file_path);

    if (!bitmap) {
        ALLEGRO_FONT *fnt = al_create_builtin_font();
        bitmap = al_create_bitmap(64,64);
        al_set_target_bitmap(bitmap);
        al_clear_to_color(al_map_rgb(255,255,255));
        al_draw_text(fnt, al_map_rgb(255,0,0), 0, 0, 0, "ERROR!");
        al_set_target_bitmap(al_get_backbuffer(window_get_display()));
        al_destroy_font(fnt);
    }
    else {
        al_convert_mask_to_alpha(bitmap, al_map_rgb(255,0,255));
    }

    return bitmap;
}

// Spritesheet

void spritesheet_init(spritesheet* spritesheet, const char* bitmap_file_path) {
    spritesheet->bitmap = safe_load_image(bitmap_file_path);
}

void spritesheet_destroy(spritesheet* spritesheet) {
    al_destroy_bitmap(spritesheet->bitmap);
}

sprite_animation_frame spritesheet_get_frame(spritesheet* spritesheet, spritesheet_frame_info frame_info) {
    sprite_animation_frame frame = {
        .bitmap = spritesheet->bitmap,
        .x = frame_info.x,
        .y = frame_info.y,
        .anchor_x = frame_info.anchor_x,
        .anchor_y = frame_info.anchor_y,
        .width = frame_info.width,
        .height = frame_info.height,
        .duration = frame_info.duration
    };
    return frame;
}

sprite_animation spritesheet_get_animation(spritesheet* spritesheet, const char* animation_name, int frame_count, spritesheet_frame_info* frames) {
    sprite_animation animation;
    sprite_animation_init_c(&animation, animation_name, frame_count);
    for (int i = 0; i < frame_count; i++) {
        sprite_animation_add_frame(&animation, spritesheet_get_frame(spritesheet, frames[i]));
    }
    return animation;
}

sprite_animation* spritesheet_add_animation(spritesheet* spritesheet, sprite* sprite, const char* animation_name, int frames_count, spritesheet_frame_info* frames) {
    return sprite_add_animation(sprite, spritesheet_get_animation(spritesheet, animation_name, frames_count, frames));
}

// Grid Spritesheet

void spritesheet_grid_init_marged_spaced(spritesheet_grid* spritesheet, const char* bitmap_file_path, int columns, int rows, spritesheet_grid_margin margin, int spacing_x, int spacing_y) {
    spritesheet->bitmap = safe_load_image(bitmap_file_path);
    spritesheet->bitmap_size_x = al_get_bitmap_width(spritesheet->bitmap);
    spritesheet->bitmap_size_y = al_get_bitmap_height(spritesheet->bitmap);
    spritesheet->frame_distance_x = (spritesheet->bitmap_size_x - margin.left - margin.right) / columns;
    spritesheet->frame_distance_y = (spritesheet->bitmap_size_y - margin.top - margin.bottom) / rows;
    spritesheet->frame_size_x = spritesheet->frame_distance_x - spacing_x;
    spritesheet->frame_size_y = spritesheet->frame_distance_x - spacing_y;
    spritesheet->margin = margin;
    spritesheet->columns = columns;
    spritesheet->rows = rows;
}

void spritesheet_grid_init_marged(spritesheet_grid* spritesheet, const char* bitmap_file_path, int columns, int rows, spritesheet_grid_margin margin) {
    spritesheet_grid_init_marged_spaced(spritesheet, bitmap_file_path, columns, rows, margin, 0, 0);
}

void spritesheet_grid_init_spaced(spritesheet_grid* spritesheet, const char* bitmap_file_path, int columns, int rows, int spacing_x, int spacing_y) {
    spritesheet_grid_margin margin = { .left = 0, .top = 0, .right = 0, .bottom = 0 };
    spritesheet_grid_init_marged_spaced(spritesheet, bitmap_file_path, columns, rows, margin, spacing_x, spacing_y);
}

void spritesheet_grid_init(spritesheet_grid* spritesheet, const char* bitmap_file_path, int columns, int rows) {
    spritesheet_grid_margin margin = { .left = 0, .top = 0, .right = 0, .bottom = 0 };
    spritesheet_grid_init_marged_spaced(spritesheet, bitmap_file_path, columns, rows, margin, 0, 0);
}

sprite_animation_frame spritesheet_grid_get_frame(spritesheet_grid* spritesheet, spritesheet_grid_frame_info frame_info) {
    sprite_animation_frame frame = {
        .bitmap = spritesheet->bitmap,
        .x = spritesheet->margin.left + frame_info.column * spritesheet->frame_distance_x,
        .y = spritesheet->margin.top + frame_info.row * spritesheet->frame_distance_y,
        .anchor_x = frame_info.anchor_x,
        .anchor_y = frame_info.anchor_y,
        .width = spritesheet->frame_size_x,
        .height = spritesheet->frame_size_y,
        .duration = frame_info.duration
    };
    return frame;
}

sprite_animation spritesheet_grid_get_animation(spritesheet_grid* spritesheet, const char* animation_name, int frame_count, spritesheet_grid_frame_info* frames) {
    sprite_animation animation;
    sprite_animation_init_c(&animation, animation_name, frame_count);
    for (int i = 0; i < frame_count; i++) {
        sprite_animation_add_frame(&animation, spritesheet_grid_get_frame(spritesheet, frames[i]));
    }
    return animation;
}

sprite_animation* spritesheet_grid_add_animation(spritesheet_grid* spritesheet, sprite* sprite, const char* animation_name, int frames_count, spritesheet_grid_frame_info* frames) {
    return sprite_add_animation(sprite, spritesheet_grid_get_animation(spritesheet, animation_name, frames_count, frames));
}

// Image loader

void imageloader_frame_init(imageloader_frame_loaded* frame, imageloader_frame_info frame_info) {
    frame->bitmap = safe_load_image(frame_info.bitmap_file_path);
    g_strlcpy(frame->name, frame_info.bitmap_file_path, IMAGELOADER_SPRITE_NAME_MAX_LENGTH - 1);
    frame->bitmap_size_x = al_get_bitmap_width(frame->bitmap);
    frame->bitmap_size_y = al_get_bitmap_height(frame->bitmap);
}

void imageloader_frame_destroy(imageloader_frame_loaded* frame) {
    al_destroy_bitmap(frame->bitmap);
}

void imageloader_init_c(imageloader* imageloader, int frames_initial_capacity) {
    imageloader->frames = malloc(sizeof(imageloader_frame_loaded) * frames_initial_capacity);
    imageloader->frames_capacity = frames_initial_capacity;
    imageloader->frames_count = 0;
}

void imageloader_init(imageloader* imageloader) {
    imageloader_init_c(imageloader, IMAGELOADER_DEFAULT_FRAMES_CAPACITY);
}

void imageloader_destroy(imageloader* imageloader) {
    for (int i = 0; i < imageloader->frames_count; i++) {
        imageloader_frame_destroy(&imageloader->frames[i]);
    }
}

sprite_animation_frame imageloader_get_frame(imageloader* imageloader, imageloader_frame_info frame) {
    imageloader_frame_loaded* frame_loaded = NULL;
    for (int i = 0; i < imageloader->frames_count; i++) {
        if (strncmp(imageloader->frames[i].name, frame.bitmap_file_path, IMAGELOADER_SPRITE_NAME_MAX_LENGTH) == 0) {
            frame_loaded = &imageloader->frames[i];
            break;
        }
    }
    if (frame_loaded == NULL) {
        if (imageloader->frames_count + 1 > imageloader->frames_capacity) {
            if (imageloader->frames_capacity <= 0) imageloader->frames_capacity = 1;
            else imageloader->frames_capacity *= 2;
            imageloader->frames = realloc(imageloader->frames, sizeof(imageloader_frame_loaded) * imageloader->frames_capacity);

            if (imageloader->frames == NULL)
                exit (EXIT_FAILURE);
        }

        frame_loaded = &imageloader->frames[imageloader->frames_count];

        imageloader_frame_init(frame_loaded, frame);
        imageloader->frames_count += 1;
    }

    return (sprite_animation_frame) {
        .bitmap = frame_loaded->bitmap,
        .x = 0,
        .y = 0,
        .anchor_x = frame.anchor_x,
        .anchor_y = frame.anchor_y,
        .width = frame_loaded->bitmap_size_x,
        .height = frame_loaded->bitmap_size_y,
        .duration = frame.duration
    };
}

sprite_animation imageloader_get_animation(imageloader* imageloader, const char* animation_name, int frame_count, imageloader_frame_info* frames) {
    sprite_animation animation;
    sprite_animation_init_c(&animation, animation_name, frame_count);
    for (int i = 0; i < frame_count; i++) {
        sprite_animation_add_frame(&animation, imageloader_get_frame(imageloader, frames[i]));
    }
    return animation;
}

sprite_animation* imageloader_add_animation(imageloader* imageloader, sprite* sprite, const char* animation_name, int frames_count, imageloader_frame_info* frames) {
    return sprite_add_animation(sprite, imageloader_get_animation(imageloader, animation_name, frames_count, frames));
}

void sprite_init_image(sprite* sprite, imageloader_frame_info frame) {
    static imageloader imageloader = { .frames_capacity = 0 };

    if (imageloader.frames_capacity == 0) {
        imageloader_init(&imageloader);
    }

    sprite_init_c(sprite, 1);
    imageloader_add_animation(&imageloader, sprite, "default", 1, (imageloader_frame_info[]) { frame });
}

void sprite_init_image_path(sprite* sprite, const char* image_path) {
    sprite_init_image(sprite, (imageloader_frame_info) { .bitmap_file_path = image_path });
}

const json_value* json_find_property(const json_value* json, const char* property_name)
{
    if (json->type != json_object)
        return &json_value_none;

    for (unsigned int i = 0; i < json->u.object.length; ++ i)
        if (!strcmp (json->u.object.values [i].name, property_name))
            return json->u.object.values [i].value;

    return &json_value_none;
}

double json_get_double(const json_value* json)
{
    switch (json->type)
    {
        case json_double: return json->u.dbl;
        case json_integer: return json->u.integer;
        default: return 0;
    }
}

void sprite_init_json_path(sprite* sprite, const char* json_file_path)
{
    FILE* json_file = fopen(json_file_path, "rb");
    fseek(json_file, 0, SEEK_END);
    long json_file_size = ftell(json_file);
    fseek(json_file, 0, SEEK_SET);

    json_char* json_file_content = malloc(json_file_size + 1);
    fread(json_file_content, json_file_size, 1, json_file);
    fclose(json_file);

    json_file_content[json_file_size] = '\0';

    json_value* json_root = json_parse (json_file_content, json_file_size);
    free(json_file_content);

    const json_value* json_root_margin = json_find_property(json_root, "margin");
    const json_value* json_root_spacing = json_find_property(json_root, "spacing");
    const json_value* json_root_animations = json_find_property(json_root, "animations");

    char* bitmap_described_file_path = json_find_property(json_root, "bitmap")->u.string.ptr;
    char bitmap_file_path[50];

    strcpy(bitmap_file_path, json_file_path);

    char* bitmap_file_path_dir = strrchr(bitmap_file_path, '/');
    if (!bitmap_file_path_dir) {
        bitmap_file_path_dir = bitmap_file_path;
    }
    else {
        bitmap_file_path_dir += 1;
    }
    strcpy(bitmap_file_path_dir, bitmap_described_file_path);

    int rows = json_find_property(json_root, "rows")->u.integer;
    int columns = json_find_property(json_root, "columns")->u.integer;
    int spacing_x = json_find_property(json_root_spacing, "x")->u.integer;
    int spacing_y = json_find_property(json_root_spacing, "y")->u.integer;

    if (rows <= 0) rows = 1;
    if (columns <= 0) columns = 1;

    spritesheet_grid_margin margin = {
        .left = json_find_property(json_root_margin, "left")->u.integer,
        .top = json_find_property(json_root_margin, "top")->u.integer,
        .right = json_find_property(json_root_margin, "right")->u.integer,
        .bottom = json_find_property(json_root_margin, "bottom")->u.integer
    };

    spritesheet_grid grid;
    spritesheet_grid_init_marged_spaced(&grid, bitmap_file_path, columns, rows, margin, spacing_x, spacing_y);
    spritesheet sheet = { .bitmap = grid.bitmap };

    if (json_root_animations->u.array.length <= 0) {
        // Creating single frame animation
        const json_value* json_anchor = json_find_property(json_root, "anchor");

        sprite_init_c(sprite, 1);
        sprite_animation animation;
        sprite_animation_init_c(&animation, "default", rows * columns);

        int x = json_find_property(json_root, "x")->u.integer;
        int y = json_find_property(json_root, "y")->u.integer;

        int width = json_find_property(json_root, "width")->u.integer;
        int height = json_find_property(json_root, "height")->u.integer;

        float anchor_x = json_get_double(json_find_property(json_anchor, "x"));
        float anchor_y = json_get_double(json_find_property(json_anchor, "y"));

        double duration = json_get_double(json_find_property(json_root, "duration"));

        if (!x && !y && !width && !height) {
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    sprite_animation_frame frame = spritesheet_grid_get_frame(&grid, (spritesheet_grid_frame_info){
                        .column = j,
                        .row = i,
                        .anchor_x = anchor_x,
                        .anchor_y = anchor_y,
                        .duration = duration
                    });
                    sprite_animation_add_frame(&animation, frame);
                }
            }
        }
        else {
            sprite_animation_frame frame = spritesheet_get_frame(&sheet, (spritesheet_frame_info){
                .x = x,
                .y = y,
                .width = width,
                .height = height,
                .anchor_x = anchor_x,
                .anchor_y = anchor_y,
                .duration = duration
            });
            sprite_animation_add_frame(&animation, frame);
        }

        sprite_add_animation(sprite, animation);
    }
    else {
        sprite_init_c(sprite, json_root_animations->u.array.length);

        for (unsigned int i = 0; i < json_root_animations->u.array.length; i++) {

            const json_value* json_animation = json_root_animations->u.array.values[i];
            const json_value* frames = json_find_property(json_animation, "frames");
            const json_value* flags = json_find_property(json_animation, "flags");
            const char* animation_name = json_find_property(json_animation, "name")->u.string.ptr;

            sprite_animation animation;
            sprite_animation_init_c(&animation, animation_name, frames->u.array.length);

            const json_value* json_flag_repeat = json_find_property(flags, "repeat");
            if (json_flag_repeat != &json_value_none) {
                animation.flags.repeat &= json_flag_repeat->u.boolean;
            }

            for (unsigned int j = 0; j < frames->u.array.length; j++) {
                const json_value* json_frame = frames->u.array.values[j];
                const json_value* anchor = json_find_property(json_frame, "anchor");

                int x = json_find_property(json_frame, "x")->u.integer;
                int y = json_find_property(json_frame, "y")->u.integer;
                int width = json_find_property(json_frame, "width")->u.integer;
                int height = json_find_property(json_frame, "height")->u.integer;
                int row = json_find_property(json_frame, "row")->u.integer;
                int column = json_find_property(json_frame, "column")->u.integer;
                double duration = json_get_double(json_find_property(json_frame, "duration"));

                float anchor_x = json_get_double(json_find_property(anchor, "x"));
                float anchor_y = json_get_double(json_find_property(anchor, "y"));

                if (!x && !y && !width && !height) {
                    sprite_animation_frame frame = spritesheet_grid_get_frame(&grid, (spritesheet_grid_frame_info){
                        .column = column,
                        .row = row,
                        .anchor_x = anchor_x,
                        .anchor_y = anchor_y,
                        .duration = duration
                    });
                    sprite_animation_add_frame(&animation, frame);
                }
                else {
                    sprite_animation_frame frame = spritesheet_get_frame(&sheet, (spritesheet_frame_info){
                        .x = x,
                        .y = y,
                        .width = width,
                        .height = height,
                        .anchor_x = anchor_x,
                        .anchor_y = anchor_y,
                        .duration = duration
                    });
                    sprite_animation_add_frame(&animation, frame);
                }
            }

            sprite_add_animation(sprite, animation);
        }
    }

    json_value_free(json_root);
}
