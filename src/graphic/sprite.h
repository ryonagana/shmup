#ifndef _SPRITE_HEADER_
#define _SPRITE_HEADER_

#include "../utils/utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <allegro5/allegro5.h>

#define ANIMATION_NAME_MAX_LENGTH 20
#define SPRITE_ANIMATION_DEFAULT_FRAMES_CAPACITY 4
#define SPRITE_DEFAULT_ANIMATIONS_CAPACITY 4
#define IMAGELOADER_SPRITE_NAME_MAX_LENGTH 20
#define IMAGELOADER_DEFAULT_FRAMES_CAPACITY 4

// Sprite Animation

typedef union animation_all_flags {
    uint8_t  all_flags;
    struct {
        uint8_t repeat : 1;
    };
} animation_flags;

typedef struct sprite_animation_frame {
    ALLEGRO_BITMAP *bitmap;
    float x;
    float y;
    float anchor_x;
    float anchor_y;
    int width;
    int height;
    double duration;
} sprite_animation_frame;

typedef struct sprite_animation {
    char name[ANIMATION_NAME_MAX_LENGTH];
    sprite_animation_frame* frames;
    int frames_capacity;
    int frames_count;
    int repeat_at_frame_index;
    animation_flags flags;
} sprite_animation;

// Sprite

typedef union sprite_all_flags {
    uint8_t  all_flags;
    struct {
        uint8_t paused : 1;
    };
} sprite_flags;

typedef struct sprite {
    sprite_flags flags;
    sprite_animation* animations;
    int animations_capacity;
    int animations_count;
    sprite_animation* current_animation;
    int current_frame_index;
    double current_frame_elapsed_time;
    void (*before_destroy)(struct sprite*);
} sprite;

void sprite_animation_init(sprite_animation* animation, const char* name);
void sprite_animation_init_c(sprite_animation* animation, const char* name, int frames_initial_capacity);
void sprite_animation_destroy(sprite_animation* animation);

sprite_animation_frame* sprite_animation_add_frame(sprite_animation* animation, sprite_animation_frame frame);

void sprite_init_json_path(sprite* sprite, const char* json_file_path);

void sprite_init(sprite* sprite);
void sprite_init_c(sprite* sprite, int animations_initial_capacity);
void sprite_destroy(sprite* sprite);

sprite_animation* sprite_add_animation(sprite* sprite, sprite_animation animation);

void sprite_play_animation(sprite* sprite, const char* animation_name);
void sprite_set_paused(sprite* sprite, int paused);

int sprite_get_width(sprite* sprite);
int sprite_get_height(sprite* sprite);

void sprite_update(sprite* sprite, ALLEGRO_EVENT* e);
void sprite_draw(sprite* sprite, float x, float y);
void sprite_draw_tinted(sprite* sprite, float x, float y, ALLEGRO_COLOR color);
void sprite_draw_rotate(sprite* sprite, float pivot_x, float pivot_y, float x, float y, float angle);
void sprite_draw_rotate_tinted(sprite* sprite, ALLEGRO_COLOR tint, float pivot_x, float pivot_y, float x, float y, float angle);
// Base Spritesheet

typedef struct spritesheet {
    ALLEGRO_BITMAP *bitmap;
} spritesheet;

typedef struct spritesheet_frame_info {
    int x;
    int y;
    float anchor_x;
    float anchor_y;
    int width;
    int height;
    double duration;
} spritesheet_frame_info;

void spritesheet_init(spritesheet* spritesheet, const char* bitmap_file_path);
void spritesheet_destroy(spritesheet* spritesheet);

sprite_animation_frame spritesheet_get_frame(spritesheet* spritesheet, spritesheet_frame_info frame_info);
sprite_animation spritesheet_get_animation(spritesheet* spritesheet, const char* animation_name, int frame_count, spritesheet_frame_info* frames);

sprite_animation* spritesheet_add_animation(spritesheet* spritesheet, sprite* sprite, const char* animation_name, int frames_count, spritesheet_frame_info* frames);

// Grid Spritesheet

typedef struct spritesheet_grid_margin {
    int left;
    int top;
    int right;
    int bottom;
} spritesheet_grid_margin;

typedef struct spritesheet_grid {
    ALLEGRO_BITMAP *bitmap;
    int bitmap_size_x;
    int bitmap_size_y;
    int frame_distance_x;
    int frame_distance_y;
    int frame_size_x;
    int frame_size_y;
    int columns;
    int rows;
    spritesheet_grid_margin margin;
} spritesheet_grid;

typedef struct spritesheet_grid_frame_info {
    int column;
    int row;
    float anchor_x;
    float anchor_y;
    double duration;
} spritesheet_grid_frame_info;

void spritesheet_grid_init_marged_spaced(spritesheet_grid* spritesheet, const char* bitmap_file_path, int columns, int rows, spritesheet_grid_margin margin, int spacing_x, int spacing_y);
void spritesheet_grid_init_marged(spritesheet_grid* spritesheet, const char* bitmap_file_path, int columns, int rows, spritesheet_grid_margin margin);
void spritesheet_grid_init_spaced(spritesheet_grid* spritesheet, const char* bitmap_file_path, int columns, int rows, int spacing_x, int spacing_y);
void spritesheet_grid_init(spritesheet_grid* spritesheet, const char* bitmap_file_path, int columns, int rows);
void spritesheet_grid_destroy(spritesheet_grid* spritesheet);

sprite_animation_frame spritesheet_grid_get_frame(spritesheet_grid* spritesheet, spritesheet_grid_frame_info frame_description);
sprite_animation spritesheet_grid_get_animation(spritesheet_grid* spritesheet, const char* animation_name, int frame_count, spritesheet_grid_frame_info* frames);

sprite_animation* spritesheet_grid_add_animation(spritesheet_grid* spritesheet, sprite* sprite, const char* animation_name, int frames_count, spritesheet_grid_frame_info* frames);

// Image Loader

typedef struct imageloader_frame_info {
    const char* bitmap_file_path;
    float anchor_x;
    float anchor_y;
    double duration;
} imageloader_frame_info;

typedef struct imageloader_frame_loaded {
    ALLEGRO_BITMAP *bitmap;
    int bitmap_size_x;
    int bitmap_size_y;
    char name[IMAGELOADER_SPRITE_NAME_MAX_LENGTH];
} imageloader_frame_loaded;

typedef struct imageloader {
    imageloader_frame_loaded* frames;
    int frames_count;
    int frames_capacity;
} imageloader;

void imageloader_init(imageloader* imageloader);
void imageloader_init_c(imageloader* imageloader, int sprites_initial_capacity);
void imageloader_destroy(imageloader* imageloader);

sprite_animation_frame imageloader_get_frame(imageloader* imageloader, imageloader_frame_info frame);
sprite_animation imageloader_get_animation(imageloader* imageloader, const char* animation_name, int frame_count, imageloader_frame_info* frames);

sprite_animation* imageloader_add_animation(imageloader* imageloader, sprite* sprite, const char* animation_name, int frames_count, imageloader_frame_info* frames);

void sprite_init_image(sprite* sprite, imageloader_frame_info frame);
void sprite_init_image_path(sprite* sprite, const char* image_path);

#endif
