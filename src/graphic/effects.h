#ifndef EFFECTS_HEADER
#define EFFECTS_HEADER
#include <allegro5/allegro5.h>

#define READY_TIME  800
#define READY_STATE_NONE 0
#define READY_STATE_RDY  1
#define READY_STATE_GO   2
#define READY_STATE_END  3

#define SHAKE_TIME  (100)
#define SHAKE_TIMES_SCREEN (10)


void shake_screen(void);
void shake_screen_update(int step);
void shake_screen_render(void);
int  shake_get_counter(void);


void ready_text(void);
int ready_update(void);
void ready_render(void);





#endif
