#include "fade.h"
#include "../window/window.h"
#include "../utils/utils.h"
#include <math.h>

static ALLEGRO_BITMAP *fade_bitmap = NULL;


static void fade_in_process(fade_t *fx);
static void fade_out_process(fade_t *fx);

static fade_t fade_effect = {
    0,
    0.0f,
    FADE_UNUSED
};

void fade_init(void){

    if(fade_bitmap == NULL){
        fade_bitmap = al_create_bitmap(window_get_width(), window_get_height());
    }

    al_set_target_bitmap(fade_bitmap);
    al_clear_to_color(al_map_rgba_f(0.0f,0.0f,0.0f,1.0));
    al_set_target_backbuffer(g_display);

}


void fade_update(void){
    if(fade_effect.counter >= 0 ){
        switch(fade_effect.type){
            case FADE_IN:
            fade_in_process(&fade_effect);
            break;

            case FADE_OUT:
            fade_out_process(&fade_effect);
            break;

            case FADE_UNUSED:
            break;

        }
    }
}



static void fade_in_process(fade_t *fx){
    if( window_get_milliseconds() < fx->counter  ){
       fx->alpha -= 0.01;
       fx->type = FADE_OUT;
    }else {
        fx->type = FADE_IN;
    }
}
static void fade_out_process(fade_t *fx){
    if( window_get_milliseconds() < fx->counter  ){
       fx->alpha += 0.01;
       fx->type = FADE_IN;
    }else {
        fx->type = FADE_OUT;
    }
}

void fade_render(void){
    al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);
    al_draw_tinted_bitmap(fade_bitmap, al_map_rgba_f(0.0f,0.0f,0.0f, fade_effect.alpha), 0,0, 0);
    al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);
}

void fade_in(int time){
    fade_effect.type = FADE_IN;
    fade_effect.counter = (al_get_timer_count(timer_ingame) * 100) + time;
}
void fade_out(int time){
    fade_effect.type = FADE_OUT;
    fade_effect.counter = (al_get_timer_count(timer_ingame) * 100) + time;
}

void fade_destroy(void){
    if(fade_bitmap != NULL) al_destroy_bitmap(fade_bitmap);
}
