#include "player.h"
#include "input/keyboard.h"
#include "window/window.h"
#include "utils/debug.h"
#include "graphic/sprite.h"
#include "utils/math_helper.h"
#include <allegro5/allegro_primitives.h>
#include "utils/xalloc.h"
#include "sound_res.h"

Player player;


static sprite ship;
static sprite projectile_sprite;


Shot shotList[MAX_SHOTS];

static void player_move(Player *p, float x, float y){

    p->position.x += x * p->acceleration.x;
    p->position.y += y * p->acceleration.y;
}

static GRect* player_update_rect_values(Player *p, GRect *rect){
    sprite_animation_frame *actualFrame = NULL;
    actualFrame = &ship.current_animation->frames[ship.current_frame_index];

    rect->h = p->rect.h = p->position.y + actualFrame->height;
    rect->w = p->rect.w = p->position.x + actualFrame->height;
    rect->x = p->position.x;
    rect->y = p->position.y;

    return rect;
}


void player_init(){
    vector_Zero(&(player.position));
    vector_Zero(&player.acceleration);
    player.flags.all = 0;
    player.actual_weapon = WEAPON_DEF_WCANNON;
    player.rotation = 90;
    player.acceleration.x = 3.0;
    player.acceleration.y = 3.0;


    sprite_init_json_path(&ship, "assets/ship.sprite");
    shot_start(shotList, &projectile_sprite, "assets/shot01.sprite");

    sprite_animation_frame *actualFrame = NULL;
    actualFrame = &ship.current_animation->frames[ship.current_frame_index];


    player.rect.w = player.position.x + actualFrame->width;
    player.rect.h = player.position.y + actualFrame->height;
    player.rect.x = player.position.x;
    player.rect.y = player.position.y;
}


void player_update(ALLEGRO_EVENT *e){

    (void) e;

    if(keyboard_keypressed(ALLEGRO_KEY_W) ){

        player_move(&player, 0, -1);
    }else if(keyboard_keypressed(ALLEGRO_KEY_S)){

        player_move(&player, 0, 1);
    }

    if(keyboard_keypressed(ALLEGRO_KEY_A)){

        player_move(&player, -1, 0);

    }else if(keyboard_keypressed(ALLEGRO_KEY_D)){

        player_move(&player, 1, 0);
    }

    if(mInput.lButton && (player.flags.is_shooting & 1)){

    }


}

void player_update_input(ALLEGRO_EVENT *e){


    if(e->type == ALLEGRO_EVENT_KEY_DOWN) keys[e->keyboard.keycode] = 1;
    if(e->type == ALLEGRO_EVENT_KEY_UP) keys[e->keyboard.keycode] = 0;

    if(e->type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN){

        if(e->mouse.button & 1){
            mInput.lButton = 1;
        }

        if(e->mouse.button & 2){
            mInput.rButton = 1;
        }
    }


    if(e->type == ALLEGRO_EVENT_MOUSE_BUTTON_UP){
        if(e->mouse.button & 1){
            mInput.lButton = 0;
        }

        if(e->mouse.button & 2){
            mInput.rButton = 0;
        }
    }

}

void player_render(void){
    sprite_animation_frame *current_frame;

    current_frame = NULL;

    if((player.flags.visible & 1) && (player.flags.is_alive & 1) ){
        current_frame =  &ship.current_animation->frames[ship.current_frame_index];
        sprite_draw(&ship, player.position.x, player.position.y);

        //sprite_draw_rotate(&ship, current_frame->width / 2, current_frame->height / 2, player.position.x, player.position.y, player.rotation);

    }
}

void player_spawn(float x, float y){
    if(player.flags.is_alive) {
        DPRINT("Player is Alive he Can't Spawn Again!","");
        return;
    }

    player.position.x = window_get_width() - x;
    player.position.y = window_get_width() - y;
    player.flags.is_alive |= 1;
    player.flags.visible |= 1;
    player.flags.invincible |= 0;
    player.lives = 3;
}
