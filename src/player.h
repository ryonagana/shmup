#ifndef PLAYER_HEADER
#define PLAYER_HEADER

#include <allegro5/allegro5.h>
#include "utils/type.h"

#include "utils/vector2.h"
#include "utils/collision.h"
#include "gun.h"
#include "shot.h"

typedef union player_flags_t {
    uint32 all;

    struct {
        uint32 visible  : 1,
               invincible : 1,
               is_shooting : 1,
               is_alive : 1;
    };

}player_flags_t;


typedef struct Player {
    Vector2 position;
    Vector2 acceleration;
    player_flags_t flags;
    WeaponConfig weapons[MAX_WEAPONS + 1];
    f32 rotation;
    sint actual_weapon;
    sint lives;
    GRect rect;

}Player;

extern Player player;


void player_init(void);
void player_update(ALLEGRO_EVENT *e);
void player_update_input(ALLEGRO_EVENT *e);
void player_render(void);

void player_spawn(float x, float y);

extern Shot shotList[MAX_SHOTS];
#endif
