#include "shot.h"
#include "gun.h"
#include "sound_res.h"
#include "utils/collision.h"
#include "enemy.h"
#include <allegro5/allegro_primitives.h>
#include "utils/vector2.h"
#include "utils/debug.h"


static void shot_init_single(Shot *b, int w, int h){
    b->alive = FALSE;
    b->delay = 0;

    vector_Zero(&b->pos);
    vector_Zero(&b->accel);

    b->w = w;
    b->h = h;
}


void shot_set(Shot *shot, const Vector2 *dest, const Vector2 *accel, int w, int h,  int delay){
    shot->alive = FALSE;
    shot->pos = *dest;
    shot->accel = *accel;
    shot->delay = delay;
    shot->type = WEAPON_DEF_WCANNON;
    shot->w = w;
    shot->h = h;

}

void shot_start(Shot bullet_list[MAX_SHOTS], sprite *anim_sprite, const char* sprite_path) {
    int i;
    sprite_init_json_path(anim_sprite, sprite_path);

    /* init all slots */
    for(i = 0; i < MAX_SHOTS; i++){
        shot_init_single(&bullet_list[i], sprite_get_width(anim_sprite), sprite_get_height(anim_sprite));
    }
}

int shot_collide_enemy(enemy *e, Shot *s){
    GRect a,b;

    a.w = e->w;
    a.h = e->h;
    a.x = e->position.x +  e->w;
    a.y = e->position.y +  e->h;

    b.h = s->h;
    b.w = s->w;
    b.x = s->pos.x + s->w;
    b.y = s->pos.y + s->h;


    return aabb_collision(&a,&b);
}


int shot_dmg_enemy(enemy *en, int hitpoints){

    en->life -= hitpoints;
    return en->life;
}


void shot_update(Shot shots[MAX_SHOTS]){
    int i;

    for(i = 0; i < MAX_SHOTS; i++){
        if(shots[i].alive){

            if((int) shots[i].pos.y + 50 <= 0){
                shots[i].alive = FALSE;
                continue;
            }
                shots[i].pos.x += shots[i].accel.x;
                shots[i].pos.y += shots[i].accel.y;
                shots[i].rect.x  =  shots[i].pos.x;
                shots[i].rect.y  =  shots[i].pos.y;
                shots[i].angle = 0.0;
        }
    }
}

Shot* shot_get_free_slot(Shot shots[MAX_SHOTS]){
    int c =0;
    while(shots[c].alive && c < MAX_SHOTS) c++;
    if(c == MAX_SHOTS) return NULL;
    return &shots[c];
}


void shot_projectile(Shot shots[MAX_SHOTS], const Vector2 *initial_pos, const Vector2 *accel){

        Vector2 dest_pos;
        Vector2 dest_accel;
        Shot *shot = NULL;

        vector_Zero(&dest_pos);
        vector_Zero(&dest_accel);

        shot = shot_get_free_slot(shots);

        dest_pos.x = initial_pos->x;
        dest_pos.y = initial_pos->y;



        vector_Set(&dest_accel, accel);


        vector_Set(&(shot->pos), &dest_pos);

        dest_accel.x = 0.0;
        dest_accel.y += -5.0;

        vector_Set(&(shot->accel), &dest_accel);

        shot->alive = TRUE;
        shot->delay = shot->delay;
        /* type of the gun (change sprite and fire mode, not gun properties dmg) */

        shot->type = 0; /* 0 gun mode, 1 laser, 2 vulcano shot, 3 multi shot, 4 homing missile */



}
void shot_render(Shot shots[MAX_SHOTS], sprite *spr){
            int i;

            for(i = 0; i < MAX_SHOTS; i++){
                if(shots[i].alive){
                    sprite_draw(spr, shots[i].pos.x, shots[i].pos.y );

                }

            }

}
