#include "enemy.h"
#include "window/window.h"
#include "utils/math_helper.h"
#include <allegro5/allegro_primitives.h>
#include "window/scene.h"
#include "gameover.h"

//extern controlled_player player[2];



enemy enemy_list[MAX_ENEMIES];
int enemy_count;

static ALLEGRO_BITMAP* enemy_sprite[MAX_ENEMY_SPRITE];
static void enemy_set_default_flags(enemy *e);
static void enemy_brain_enemyx(enemy *e, Player *p);



static void enemy_enemyx_idle(void *data);


void enemy_init(void)
{
	int i;
	
    //memset(enemy_list,0x0, sizeof(enemy_list));
	
    //enemy_sprite[ENEMY_X] =  al_load_bitmap("assets//ship.bmp");
	
    if(!enemy_sprite[ENEMY_X]){
        enemy_sprite[ENEMY_X] = al_create_bitmap(120,40);

        SET_TARGET(enemy_sprite[ENEMY_X]);
        al_clear_to_color(al_map_rgb(0,0,255));
        RESET_DISPLAY();
    }
	
    for (i = 0; i < MAX_ENEMIES; i++){

            vector_Zero(&enemy_list[i].position);
            enemy_list[i].type = ENEMY_X;
            enemy_list[i].angle = 1.0f;
            enemy_list[i].counter = 0;
            enemy_list[i].flags.all_flags = 0U;
            enemy_list[i].flags.alive |= 0;
            enemy_list[i].flags.invincible |= 0;
            enemy_list[i].flags.updated |= 0;
            enemy_list[i].flags.visible |= 0;
            enemy_list[i].brain = NULL;
            enemy_list[i].state = ENEMY_STATE_IDLE;

            fsm_init(&enemy_list[i].fsm);



            enum enemy_type type = enemy_list[i].type;

            enemy_list[i].w = al_get_bitmap_width(enemy_sprite[ (int) type]);
            enemy_list[i].h = al_get_bitmap_height(enemy_sprite[ (int) type]);

            enemy_list[i].rect.w = enemy_list[i].w;
            enemy_list[i].rect.h = enemy_list[i].h;
            enemy_list[i].rect.x = enemy_list[i].position.x;
            enemy_list[i].rect.y = enemy_list[i].position.y;

	}



}

void enemy_add(enum enemy_type type, float x, float y, float vel_x, float vel_y, int visible, void (*custom_enemy_brain)(enemy *e, Player *p)){

    enemy *enemy = &enemy_list[enemy_count];


    if(enemy_count <= MAX_ENEMIES){

        vector_Init(&enemy->position, x, y);
        enemy->position.x = x;
        enemy->position.y = y;
        enemy->type = type;
        enemy->flags.visible = (unsigned int) visible;
        enemy->flags.alive |= 1;
        enemy->accel.x = vel_x;
        enemy->accel.y = vel_y;

        enemy_set_default_flags(enemy);


        /*
         * use this switch to distribute custom values for each enemy type
         * like lif, armor value etc..
         */
        switch(enemy->type){
            case ENEMY_X:
               enemy->life = 660;

               if(custom_enemy_brain == NULL){
                   enemy->brain = enemy_brain_enemyx;
               }

            break;

            case ENEMY_SCAVENGER:
                enemy->life = 350;
            break;
        }





        enemy_count++;
    }
}



void enemy_update(ALLEGRO_EVENT *ev, Player *p)
{

    UNUSED_VAR(ev);
    UNUSED_VAR(p);

    int i;



    for(i = 0; i < MAX_ENEMIES && enemy_list[i].flags.alive & 1;i++){

         enemy *the_enemy = &enemy_list[i];


         switch (the_enemy->type) {
            case ENEMY_X:
                the_enemy->brain(the_enemy, &player);
            break;

         }



        the_enemy->rect.w = enemy_list[i].w;
        the_enemy->rect.h = enemy_list[i].h;
        the_enemy->rect.x = enemy_list[i].position.x;
        the_enemy->rect.y = enemy_list[i].position.y;

        fsm_update_state(&the_enemy->fsm, (void*) the_enemy);

    }
	
	
}

void enemy_draw(void)
{
    int i;

    for(i = 0; i < MAX_ENEMIES && enemy_list[i].flags.alive & 1;i++){
        if(enemy_list[i].type == ENEMY_X){
            al_draw_rectangle(enemy_list[i].position.x, enemy_list[i].position.y , enemy_list[i].position.x + enemy_list[i].w, enemy_list[i].position.y + enemy_list[i].h, al_map_rgb(0,255,255), 1.0);
        }


    }

}


static void enemy_brain_enemyx(enemy *e, Player *p){



    switch(e->state){
        case ENEMY_STATE_IDLE:
        fsm_set_state(&e->fsm, enemy_enemyx_idle);
        break;

        case ENEMY_STATE_PLAYER_DETECTED:
        break;

        case ENEMY_STATE_PLAYER_NOT_DETECTED:
        break;

        case ENEMY_STATE_AVOID_PLAYER:
        break;

        case ENEMY_STATE_FOLLOW_PLAYER:
        break;
    }



}

static void enemy_enemyx_idle(void *data){
    enemy *e = NULL;
    Player *player = NULL;

    e = (enemy*) data;
    player = player;




}


static void enemy_set_default_flags(enemy *e){
    e->flags.alive |= 1;
    e->flags.visible |= 1;
    e->flags.invincible |= 0;
    e->flags.updated |= 0;
    e->flags.blink |= 0;
}

void enemy_destroy(void){
    int i;
    for(i = 0; i < MAX_ENEMY_SPRITE;i++){
        if(enemy_sprite[i] != NULL ) al_destroy_bitmap(enemy_sprite[i]);
        enemy_sprite[i] = NULL;
    }
}

