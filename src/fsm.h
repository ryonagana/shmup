#ifndef FSM_HEADER
#define FSM_HEADER

#include <stdio.h>


struct FSM {
    int active;
    void (*active_state)(void *fsm_data);

};


void fsm_init(struct FSM *fsm);
void fsm_set_state(struct FSM* state, void (*state_func)(void *fsm_data));
void fsm_update_state(struct FSM* state, void *data);

#define FSM_STATE_MAX 15




#endif
