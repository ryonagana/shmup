#ifndef BULLET_HEADER
#define BULLET_HEADER

#define MAX_SHOTS (10)

#include <stdio.h>
#include "window/window.h"
#include "graphic/sprite.h"
#include "utils/collision.h"
#include "utils/vector2.h"

struct Player;
struct enemy;
typedef struct Player Player;
typedef struct enemy enemy;


typedef struct Shot {
    Vector2 pos;
    Vector2 target;
    Vector2 accel;
    GRect rect;
    int alive;
    int w;
    int h;
    int delay;
    int type;
    float speed;
    float angle;
}Shot;



void shot_start(Shot bullet_list[MAX_SHOTS], sprite *anim_sprite, const char* sprite_path);
void shot_set(Shot *shot, const Vector2 *dest, const Vector2 *accel,  int w, int h,  int delay);
void shot_update(Shot shots[MAX_SHOTS]);
void shot_render(Shot *s, sprite *spr);
void shot_projectile(Shot shots[MAX_SHOTS], const Vector2 *initial_pos, const Vector2 *accel);
int shot_collide_shot(Player *p, Shot *s);
int shot_collide_enemy(enemy *e, Shot *s);
int shot_dmg_enemy(enemy *en, int hitpoints);
Shot* shot_get_free_slot(Shot shots[MAX_SHOTS]);

#endif
