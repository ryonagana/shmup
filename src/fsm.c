#include "fsm.h"
#include "utils/utils.h"

void fsm_init(struct FSM *fsm){
    fsm->active_state = NULL;
    fsm->active = FALSE;
}

void fsm_set_state(struct FSM* state, void (*state_func)(void *fsm_data)){
    if(state->active_state == NULL) state->active_state = state_func;
    return;
}


void fsm_update_state(struct FSM* state, void *data){
    if(state->active_state == NULL) return;
    state->active_state(data);
    return;
}
