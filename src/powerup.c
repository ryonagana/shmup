#include "powerup.h"
#include <allegro5/allegro_primitives.h>
#include "utils/angles.h"
#include "window/window.h"
#include "utils/math_helper.h"

Powerup  powerupList[MAX_POWERUPS_SCREEN];

static sprite doubledmg_sprite;
static ALLEGRO_FONT *dbg_fnt = NULL;
static ALLEGRO_BITMAP *powerup_no_img = NULL;



static void powerup_DoubleDmg_Thk(Powerup *p, Player *player);


struct powerup_text {
    PowerupId id;
    char text[127];
};

#define MAX_POWERUP_TEXT_MESSAGE (36)

static struct powerup_text  powerup_pickup_message[MAX_POWERUP_TEXT_MESSAGE] = {

    {POWERUP_DOUBLEDMG, "Double Damage!  Kill them All!\0"},
    {POWERUP_DOUBLEDMG, "Double Damage!  You're the One!\0"},
    {POWERUP_DOUBLEDMG, "Double Damage!  Kill'em Now!\0"},
    {POWERUP_DOUBLEWEAP, "Double Weapon! Twice Emotion\0"},
    {POWERUP_DOUBLEWEAP, "Double Weapon! A la puta que te pario\0"},
    {POWERUP_DOUBLEWEAP, "Double Weapon! double kill\0"},
    {POWERUP_INVINCIBILITY, "Cheater!!!!!!!!\0"},
    {POWERUP_SHIELD, "Shield for kids!\0"},
    {POWERUP_LAST_UNUSED, "\0"},
    {POWERUP_LAST_UNUSED, "\0"}

} ;






Powerup_Modifier powerupmodList[POWERUP_LAST_UNUSED + 1]  ={
    { POWERUP_DOUBLEDMG,
      0,
      4500,
    },
    { POWERUP_DOUBLEWEAP,
      0,
      7500,
    },

    { POWERUP_SHIELD,
      0,
      9000,
    },

    { POWERUP_INVINCIBILITY,
      0,
      3000,
    },

    { POWERUP_LAST_UNUSED,
      0,
      0,
    }
};


void powerup_init(){
       int i;

       for(i = 0; i < MAX_POWERUPS_SCREEN; i++){
            powerupList[i].alive = FALSE;
            powerupList[i].bounces = 0;
            powerupList[i].Think = NULL;
            powerupList[i].time = 0;
            powerupList[i].counter = 0;
            powerupList[i].angle = RANDINT(0,270);
            powerupList[i].state = POWERUP_STATE_STOPPED;
            powerupList[i].h = 32;
            powerupList[i].w = 32;
            vector_Zero(&(powerupList[i].pos));
            vector_Zero(&(powerupList[i].accel));
       }

       dbg_fnt = al_create_builtin_font();

       Vector2 default_pos, default_accel;
       vector_Zero(&default_pos);
       vector_Zero(&default_accel);

       default_accel.x = 4.5;
       default_accel.y = 4.5;

       powerup_no_img  = al_create_bitmap(32,32);

       sprite_init_json_path(&doubledmg_sprite, "assets/bullet.sprite");


       powerup_add(POWERUP_DOUBLEDMG, &default_pos, &default_accel, 6, 3, 0, &powerup_DoubleDmg_Thk, &doubledmg_sprite);
       powerup_add(POWERUP_DOUBLEWEAP, &default_pos, &default_accel, 5,3,0, NULL, NULL);
}

/* this function is intended do insert the powerup into screen */
void powerup_put(PowerupId id,  float x, float y, float xv, float yv){
    Vector2 vec_pos;
    Vector2 accel;
    Powerup *p = NULL;

    p = &powerupList[id];

    vector_Init(&vec_pos, x,y);
    vector_Init(&accel, xv,yv);
    p->id = id;
    p->alive = TRUE;
    vector_Set(&(p->pos), &vec_pos);
    vector_Set(&(p->accel), &accel);
}

/* this is used to initialize/customize powerup values;  */
void powerup_add(PowerupId id, const Vector2 *position, const Vector2 *accel, int bounces, int time, int counter, void (*ThinkCallback)(Powerup *this, Player *p), sprite *sprite){
    Powerup *p = NULL;


    p = &powerupList[id];

    vector_Set(&(p->pos), position);
    vector_Set(&(p->accel), accel);
    p->alive = FALSE;
    p->id = id;
    p->bounces = bounces;
    p->counter = (int64_t) counter;
    p->time = (int64_t) time;
    p->Think = ThinkCallback;
    p->angle = 0.0;

}

void powerup_update(Player *p){
    int i;


    for(i = 0; i < MAX_POWERUPS_SCREEN;i++){
            if(!powerupList[i].alive) continue;

            if(powerupList[i].Think != NULL){
                    powerupList[i].Think(&powerupList[i], p);
            }

    }
}

void powerup_render(){
    int i;
    Vector2 u,v;

    vector_Zero(&u);
    vector_Zero(&v);



    for(i = 0; i < MAX_POWERUPS_SCREEN;i++){
        if(!powerupList[i].alive) continue;


        char buf[255];

        sprintf(buf, "STATE: %d BOUNCES %d", powerupList[i].state, powerupList[i].bounces);

        switch(powerupList[i].type){
            case POWERUP_DOUBLEDMG:
                al_set_target_bitmap(powerup_no_img);
                al_clear_to_color(al_map_rgb(255,255,255));
                RESET_DISPLAY();
                al_draw_rectangle(powerupList[i].pos.x, powerupList[i].pos.y,powerupList[i].pos.x + powerupList[i].w   , powerupList[i].pos.y + powerupList[i].h, al_map_rgb(0,0,255), 1.0);
                al_draw_bitmap(powerup_no_img, powerupList[i].pos.x, powerupList[i].pos.y, 0);
                al_draw_line(powerupList[i].pos.x, powerupList[i].pos.y, powerupList[i].pos.x + powerupList[i].w  * cos(powerupList[i].angle), powerupList[i].pos.y + powerupList[i].h * sin(powerupList[i].angle), al_map_rgb(0,255,0), 1.0);

            break;
        }


    }
}


static int powerup_decrease_bounces(Powerup *p){
    if(p->bounces > 0) p->bounces--;
    return p->bounces;
}

static int powerup_bounce_enabled(Powerup *p){
    if(p->bounces > 0){
        return TRUE;
    }
    return FALSE;
}


static void powerup_DoubleDmg_Thk(Powerup *p, Player *player){
    UNUSED_VAR(player);

    if(!p->alive) return;

    if(powerup_bounce_enabled(p)){
        if(p->pos.y > (window_get_height() - p->h)  ){
            p->accel.y *= -1;
            powerup_decrease_bounces(p);
        }

        if(p->pos.x > (window_get_width() - p->h)  ){
            p->accel.x *= -1;
            powerup_decrease_bounces(p);
        }


        if(p->pos.y < 0){
            p->accel.y += 1.0;
            powerup_decrease_bounces(p);
        }

        if(p->pos.x < 0){
            p->accel.x += 1.0;
            powerup_decrease_bounces(p);
        }

    }else {
        int maxW =  window_get_width() + p->h;
        int maxH = window_get_height() + p->h;

        if(p->pos.x > maxW || p->pos.x < 0 || p->pos.y > maxH || p->pos.y < 0 ){
            powerup_kill(p);
        }
    }


    p->pos.y += p->accel.y;
    p->pos.x += p->accel.x;

    p->angle = (float) getAngle(p->pos.x, p->pos.y);

}

void powerup_kill(Powerup *p){
    p->alive = FALSE;
}


int powerup_collision(Powerup *powerup, Player *thePlayer){
    GRect a,b;


    /*
    a.w = thePlayer->w;
    a.h = thePlayer->h;
    a.x = thePlayer->pos.x + (a.w / 2);
    a.y = thePlayer->pos.y + (a.h / 2);
    */

    b.w = powerup->w;
    b.h = powerup->h;
    b.x = powerup->pos.x +  (b.w / 2);
    b.y = powerup->pos.y +  (b.h / 2);


    return aabb_collision(&a,&b);


}

int powerup_shot_collision(Powerup *powerup, Shot *theShot){
    GRect a,b;

    a.x = theShot->pos.x;
    a.y = theShot->pos.y;
    a.w = (int) theShot->pos.x + theShot->w;
    a.h = (int) theShot->pos.y + theShot->h;

    b.x = powerup->pos.x;
    b.y = powerup->pos.y;
    b.w = (int) powerup->pos.x + powerup->w;
    b.h = (int) powerup->pos.y + powerup->h;


    return aabb_collision(&a,&b);

}


void powerup_pickup(GameText *text, Powerup *powerup, PowerupId id, int time, Vector2 *pos ){

    powerup->counter = *(game_timer_milliseconds) + time;
    text->x = pos->x;
    text->y = pos->y;
    int text_index;

    switch(id){
        case POWERUP_DOUBLEDMG:
            text_index = rand_int(0,2);
            if(powerup_pickup_message[text_index].id == POWERUP_LAST_UNUSED) break;
            text->text = al_ustr_newf(" ---%s---", powerup_pickup_message[ text_index].text);
        break;

        case POWERUP_INVINCIBILITY:
            text->text = al_ustr_new("Imortal?");
            break;

        case POWERUP_SHIELD:
            text->text = al_ustr_new("Shield Rocks!");
        break;

        case POWERUP_DOUBLEWEAP:
            text_index = rand_int(3,6);
            if(powerup_pickup_message[text_index].id == POWERUP_LAST_UNUSED) break;
            text->text = al_ustr_newf("%s", powerup_pickup_message[ text_index].text);
        break;

        case POWERUP_LAST_UNUSED:
            text->text = al_ustr_new("??????");
        break;


    }




}



