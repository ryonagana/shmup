#include "game.h"
#include <allegro5/allegro5.h>

#include "player.h"
#include "starfield/starfield.h"
#include "ui/ui.h"
#include "graphic/sprite.h"
#include "graphic/fade.h"
#include "enemy.h"
#include "powerup.h"
#include "shot.h"
#include "utils/text.h"
#include "graphic/particles.h"
#include "graphic/effects.h"
#include "input/inputkeys.h"
#include "sound_res.h"
#include "input/keyboard.h"
#include "input/mouse.h"

static sprite megaman;
static textData name;


static float vx = 0.2f;
static float vy = 0.1f;

static void G_init(){

    al_hide_mouse_cursor(g_display);
    player_init();
    keyboard_init();
    mouse_init(&mInput);
    gun_init();
    starfield_init();
    enemy_init();
    inputkeys_init(&name, "assets//font//vcr.ttf", 26);
    ui_start();
    fade_init();

}

static void G_start(){

    enemy_add(ENEMY_X, 400,100,1.0,1.0,TRUE, NULL);
    enemy_add(ENEMY_X, 340,160,1.0,1.0,TRUE, NULL);
    player_spawn(300,450);





    //player_spawn(&player, window_get_width() / 2 , window_get_height() - 50 );
     starfield_set_vel(vx,0);
}


static void G_update(ALLEGRO_EVENT *e){

    /*
    if(!name.confirmed) return;
    */

    player_update(e);
    shake_screen_update(100);
    starfield_update();
    ready_update();
    ui_update();
    fade_update();


    /*

    scr_fade_update();
    sprite_update(&megaman, e);
    */





}


static void G_render(){
    al_clear_to_color(al_map_rgb(25,25,25));

    /*
    if(!name.confirmed) {
        al_draw_text(name.font, al_map_rgb(255,0,0), 20,100,0, "Digite seu Nome:");
        inputkeys_set_pos(&name, 300,100);
        inputkeys_draw_shadow(&name, al_map_rgb(0,255,0), al_map_rgb(255,255,255) );
        return;
    }
    */

    shake_screen_render();
    enemy_draw();
    player_render();
    ui_render();


}

static void G_destroy(void){

    starfield_destroy();
    inputkeys_destroy(&name);
    enemy_destroy();




}



static void G_update_keyboard(ALLEGRO_EVENT *e){



    mouse_update_buttons(e);
    mouse_update_coord(e);



    /*
    if(e->type  == ALLEGRO_EVENT_KEY_CHAR){
        if(!name.confirmed){
            inputkeys_update(e, &name);
        }
    }
    */







    if ( e->keyboard.keycode == ALLEGRO_KEY_J && shake_get_counter() <= 0 ){

        fade_in(2000);
        /*
        ready_text();
        shake_screen(); */
    }

      player_update_input(e);
      starfield_set_vel(vx,vy);

}

Scene mainGame = {
    &G_init,
    &G_start,
    &G_update,
    &G_render,
    &G_update_keyboard,
    &G_destroy,
    0,
    0,
    FALSE
};
