#include "gun.h"
#include "sound_res.h"
#include "shot.h"

WeaponConfig weapon_config[MAX_WEAPONS];


void gun_set(int index, int id, int64_t timer, int dmg, const Vector2 *accel, int sfx){
    weapon_config[index].id = id;
    weapon_config[index].timer = timer * 100;
    weapon_config[index].dmg = dmg;
    weapon_config[index].accel = *accel;
    weapon_config[index].sfx = sfx;
}

void gun_init(){
    Vector2 default_accel;
    int i;

    for(i = 0; i < MAX_WEAPONS;i++){
        weapon_config[i].counter  = 0;
        weapon_config[i].dmg = 0;
        weapon_config[i].sfx = 0;
        weapon_config[i].timer = 5;
        weapon_config[i].extra_counter = 0;
        weapon_config[i].id = WEAPON_DEF_WCANNON;
        vector_Copy(&weapon_config[i].accel, &default_accel);

    }

    vector_Init(&default_accel, 10.0,0.0);

    gun_set(0, WEAPON_DEF_WCANNON, 3, 100, &default_accel, 0);
    gun_set(1, WEAPON_WLASER, 12, 100, &default_accel, 0);

}



