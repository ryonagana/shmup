#include "img_res.h"
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include "utils/resources.h"

ALLEGRO_BITMAP *ready_img = NULL;
ALLEGRO_BITMAP *go_img = NULL;





void img_res_init(void){
    ready_img = resource_load_sprite("assets//ready.bmp");
    go_img = resource_load_sprite("assets//go.bmp");

    al_convert_mask_to_alpha(ready_img, al_map_rgb(255,0,255));
    al_convert_mask_to_alpha(go_img, al_map_rgb(255,0,255));

}
void img_res_destroy(void){
    if(ready_img) al_destroy_bitmap(ready_img);
    if(go_img) al_destroy_bitmap(go_img);
}
