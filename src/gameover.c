#include "gameover.h"
#include <allegro5/allegro_font.h>
#include "window/window.h"
#include "utils/utils.h"
#include "utils/game_std.h"
#include "utils/debug.h"

static ALLEGRO_FONT *gf;
extern ALLEGRO_EVENT_QUEUE *g_queue;

static void GameOver_Init(void){
    gf = al_create_builtin_font();
}

static void GameOver_Start(){
}

static void GameOver_update(ALLEGRO_EVENT *e){
    UNUSED_VAR(e);
}
static void GameOver_render(void){
    char buf[56];
    al_clear_to_color(al_map_rgb(0,0,0));
    strlcpy(buf, "Game Over!", 56 + 1);
    al_draw_text(gf, al_map_rgb(255,255,255), 100,
                 100, 0, buf);

}
static void GameOver_destroy(void){
    al_destroy_font(gf);
}
static void GameOver_update_keyboard(ALLEGRO_EVENT *e){

    if(e->keyboard.keycode == ALLEGRO_KEY_ENTER){
        printf("APERTOU ENTER\n");
        scene_load(SCENE_MAINGAME);
        scene_reset();

    }
}

Scene sGameOver = {
    &GameOver_Init,
    &GameOver_Start,
    &GameOver_update,
    &GameOver_render,
    &GameOver_update_keyboard,
    &GameOver_destroy,
    0,
    0,
    FALSE
};

