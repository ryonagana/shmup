#include "starfield.h"
#include "../window/window.h"
#include "../utils/resources.h"
#include "../utils/utils.h"
#include "../utils/math_helper.h"
#include <allegro5/allegro_primitives.h>

extern ALLEGRO_DISPLAY *g_display; /* window.c */

typedef struct _Star {
    float x,y;
    float vel_x, vel_y;
    float rx,ry;
    int alive;
    int color;
}Star;

#define MAX_STARS 100

#define RAND_STARS(n)     ( (rand()  % n))
#define COLOR_MASK()      al_map_rgb(255,0,255)
#define GRAY_STAR_COLOR() al_map_rgb(255,0,0)

#define STAR_VEL_X 0.2f
#define STAR_VEL_Y 0.0f

typedef enum STARS_TYPE {
    STAR_BIG = 0,
    STAR_WHITE,
    STAR_RED,
    STAR_BLUE,
    STAR_MINOR
}STARS_TYPE;

static Star starList[MAX_STARS + 1];
static Star starListMiddle[MAX_STARS + 1];

static ALLEGRO_BITMAP *bmp_stars = NULL;
static ALLEGRO_BITMAP *bg_stars_gray = NULL;
static ALLEGRO_BITMAP *stars_bmp = NULL;



static int star_enabled = TRUE;
static int star_counter = 0;
static int star_timer = 2;

static int star_middle_timer = 0;


static void starfield_static_bg(void);
static void starfield_create_star(float x,float y, ALLEGRO_COLOR color);
static Star* starfield_find_empty_slot(Star sList[MAX_STARS]);
static void starfield_render_star(Star *star, int color);

static void starfield_init_list(Star list[MAX_STARS + 1]);

void starfield_init(void){
    int i;

    for(i = 0; i < MAX_STARS + 1;i++){
        starList[i].alive = FALSE;
        starList[i].color =  STAR_WHITE;
        starList[i].x = 0;
        starList[i].y = 0;
        starList[i].vel_x = STAR_VEL_X;
        starList[i].vel_y = 0;
        starList[i].rx = rand_int(1,3);
        starList[i].ry = rand_int(1,3);
    }

    bmp_stars = resource_load_sprite("assets/stars.bmp");
    al_convert_mask_to_alpha(bmp_stars, COLOR_MASK());

    bg_stars_gray = al_create_bitmap(window_get_width(), window_get_height());
    stars_bmp = al_create_bitmap(window_get_width(), window_get_height());

    starfield_static_bg();

    starfield_init_list(starList);
    starfield_init_list(starListMiddle);

    star_middle_timer = window_get_milliseconds() + 500;


}

static void starfield_init_list(Star list[MAX_STARS + 1]){
    int i;

    for(i = 0; i < MAX_STARS;i++){
        list[i].x = RAND_STARS(window_get_width());
        list[i].y = RAND_STARS(window_get_height());
        list[i].color = RAND_STARS(5);
        list[i].vel_x = 1.0;
        list[i].vel_y = 0.0;
        list[i].alive = TRUE;
    }
}


static void starfield_static_bg(){
    int i;



    al_set_target_bitmap(bg_stars_gray);
    for(i = 0; i < MAX_STARS;++i){
           float x = RAND_STARS(window_get_width() - 50);
           float y = RAND_STARS(window_get_height() - 50);
           unsigned char c = (unsigned char) RANDINT(25,76);

           starfield_create_star(x,y, al_map_rgba(c,c,c,c) );
       }

    al_set_target_bitmap(al_get_backbuffer(g_display));

}



static void starfield_create_star(float x,float y, ALLEGRO_COLOR color){
    al_draw_filled_rectangle(x,y,x + 2,y+2,color);
}


static void starfield_render_star(Star *star, int color){
    unsigned char r,g,b;
    r = RAND_STARS(255);
    g = RAND_STARS(255);
    b = RAND_STARS(255);
    float size_x = 1;
    float size_y = 1;

    switch(star->color){
        case STAR_WHITE:
             al_draw_filled_rectangle(star->x, star->y, star->x + size_x  , star->y + size_y, al_map_rgb(255,255,255));
        break;

        case STAR_RED:
            al_draw_filled_rectangle(star->x, star->y,star->x + size_x  , star->y +  size_y, al_map_rgb(255,0,0));
        break;

        case STAR_BLUE:
            al_draw_filled_rectangle(star->x, star->y, star->x + size_x  , star->y +  size_y, al_map_rgb(0,0,255));
        break;

        case STAR_MINOR:
            al_draw_filled_rectangle(star->x, star->y, star->x + size_x  , star->y +  size_y, al_map_rgb(r,g,b));
        break;

        case STAR_BIG:
            al_draw_filled_rectangle(star->x, star->y, star->x + size_x + 3  , star->y +  size_y + 3, al_map_rgb(255,255,255));
        break;


    }
}

void starfield_update(void){
    int i;


    if(star_enabled){
        star_counter++;

        if(star_counter >= star_timer){
            star_counter = 0;
        }

        for(i = 0; i < MAX_STARS && starList[i].alive;i++){

            starList[i].x -= starList[i].vel_x + rand_f(0.1,0.5);
            starList[i].y += starList[i].vel_y + rand() % 2;
            if( (int) starList[i].y > window_get_height() + 5 ){
                starList[i].alive = FALSE;
            }

            if(!starList[i].alive){

                starList[i].x = RAND_STARS(window_get_width());
                starList[i].y = - 10;
                starList[i].alive = TRUE;
                starList[i].color = RAND_STARS(5);
            }


        }

        if(window_get_milliseconds() >= star_middle_timer){
            star_middle_timer = window_get_milliseconds() + 500;
        }else {


            for(i = 0; i < MAX_STARS && starListMiddle[i].alive;i++){

                //starListMiddle[i].x -= starListMiddle[i].vel_x + rand_f(0.1,0.5);
                starListMiddle[i].y += starListMiddle[i].vel_y + rand() % 2;
                if( (int) starListMiddle[i].y > window_get_height() + 5 ){
                    starListMiddle[i].alive = FALSE;
                }

                if(!starListMiddle[i].alive){

                    starListMiddle[i].x = RAND_STARS(window_get_width());
                    starListMiddle[i].y = - 10;
                    starListMiddle[i].alive = TRUE;
                    starListMiddle[i].color = RAND_STARS(5);
                }


            }

        }

    }


}

void starfield_draw(float x, float y){
    int i;





    for(i = 0; i < MAX_STARS - 1;i++){
        //render stars  using pixel make this loop very slow due VGA card texture lock when uploaded to VRAM
        //instead pixels im using "rects" and little sprites

        starfield_render_star(&starList[i], starList[i].color);


    }

    for(i = 0; i < MAX_STARS - 1;i++){
        //render stars  using pixel make this loop very slow due VGA card texture lock when uploaded to VRAM
        //instead pixels im using "rects" and little sprites

        starfield_render_star(&starListMiddle[i], starList[i].color);


    }



    //al_draw_scaled_bitmap(bg_stars_gray,0,0,window_get_width(), window_get_height(), x,y,window_get_width(), window_get_height(), 0);





}


static Star* starfield_find_empty_slot(Star sList[MAX_STARS]){
   int c = 0;
   while(sList[c].alive && c < MAX_STARS) c++;
   if(c == MAX_STARS) return NULL;
   return &sList[c];
}

void starfield_destroy(void){
    /*if(bmp_stars) al_destroy_bitmap(bmp_stars);*/
    if(bg_stars_gray) al_destroy_bitmap(bg_stars_gray);


}

void starfield_set_vel(float x, float y){
    int i;
    for(i = 0; i < MAX_STARS;i++){
        starList[i].vel_x = x;
        starList[i].vel_y = y;
    }
}
