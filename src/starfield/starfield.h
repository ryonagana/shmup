#ifndef _STARFIELD_HEADER_
#define _STARFIELD_HEADER_

#include <stdio.h>
#include <stdlib.h>
#include <allegro5/allegro.h>


void starfield_init(void);
void starfield_update(void);
void starfield_draw(float x, float y);
void starfield_set_vel(float x, float y);
void starfield_destroy(void);

#endif // _STARFIELD_HEADER_
