#ifndef ENEMY_HEADER
#define ENEMY_HEADER

#include <stdio.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include "utils/vector2.h"
#include "enemy.h"
#include "utils/collision.h"
#include "player.h"
#include "fsm.h"

#define MAX_ENEMIES 100
#define MAX_ENEMY_SPRITE 16




enum enemy_type {
    ENEMY_X = 0,
    ENEMY_SCAVENGER,
    ENEMY_UNUSED_LAST
};

typedef union enemy_flags_t {
    uint32_t all_flags;

    struct {
        uint32_t alive : 1,
                 invincible : 1,
                 visible : 1,
                 updated : 1,
                 blink : 1;

    };


}enemy_flags_t;



enum enemy_state {
    ENEMY_STATE_IDLE=0,
    ENEMY_STATE_PLAYER_DETECTED,
    ENEMY_STATE_PLAYER_NOT_DETECTED,
    ENEMY_STATE_FOLLOW_PLAYER,
    ENEMY_STATE_AVOID_PLAYER
};



typedef struct enemy {
    Vector2 position;
    Vector2 accel;
    GRect rect;
    int w,h;
    int life;
    float angle;
    enemy_flags_t flags;
    enum enemy_state state;
    int counter;
    void (*brain)(struct enemy *e, Player *p);
    enum enemy_type type;
    struct FSM  fsm;
}enemy;


void enemy_init(void);
void enemy_add(enum enemy_type type, float x, float y, float vel_x, float vel_y, int visible, void (*custom_enemy_brain)(enemy *e, Player *p));
void enemy_update(ALLEGRO_EVENT *ev, Player *p);
void enemy_draw(void);
void enemy_destroy(void);

extern int enemy_count;
//brain

extern enemy enemy_list[MAX_ENEMIES];

#endif
