#include "tiles.h"

const char *tiles_names[TILE_LAST_BLOCK + 1] = {
    "0 Empty Tile",
    "1 Test Block",
    "Last Block( Unused Block )"
};

const char * tiles_get_name( GTilesType type){
    if( (int) type < 0) return "(empty)";
    if((int) type >= TILE_LAST_BLOCK + 1  ) return "(empty)";
    return tiles_names[type];
}
