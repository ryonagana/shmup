#ifndef TILES_HEADER
#define TILES_HEADER


typedef enum GTilesType {
    TILE_NOTHING=0,
    TILE_BASIC_BLOCK,
    TILE_LAST_BLOCK
}GTilesType;

extern const char *tiles_names[TILE_LAST_BLOCK + 1];
extern const char * tiles_get_name( GTilesType type);

#endif
