#ifndef _LEVEL_HEADER
#define _LEVEL_HEADER_

#include <stdio.h>
#include <tmx.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>


typedef struct game_map {
    tmx_map *level_map;
    tmx_layer *layer;
    ALLEGRO_BITMAP *image;
    int x_delta, y_delta;
    char name[35];

}game_map;


ALLEGRO_COLOR int_al_map_rgb(int col);
void gm_init(game_map *map, const char *levelpath);
ALLEGRO_BITMAP*  gm_render_to_bmp(tmx_map *map);
void gm_destroy(game_map *map);



#endif
