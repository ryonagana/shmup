#ifndef SOUND_RESOURCE_HEADER
#define SOUND_RESOURCE_HEADER

#include <stdio.h>
#include <allegro5/allegro5.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include "utils/mixer.h"

#define MAX_GAME_SOUNDS 127


#define SFX_PLAYER_LASER1     0
#define SFX_PLAYER_EXPLOSION2 1
#define SFX_ENEMY_HIT         2
#define SFX_ENEMY_HIT2        3
#define SFX_POWERUP01         4


void sound_resource_init(void);
void sfx_play(int id);
void sound_resource_destroy(void);


#endif
