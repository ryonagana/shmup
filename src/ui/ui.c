#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_primitives.h>
#include "ui.h"
#include "../window/window.h"
#include "../player.h"
#include "../gun.h"
#include "../utils/text.h"


static ALLEGRO_FONT *ui_font;
static ALLEGRO_USTR *text;

typedef struct  game_bars {
    float x,y;
    float w,h;
}game_bars;


static game_bars shoot_interval_bar;
static GameText lives;



void ui_start(void) {



    shoot_interval_bar.x = 50;
    shoot_interval_bar.y = window_get_height() - 100;
    shoot_interval_bar.w = 250;
    shoot_interval_bar.h = 35;

    game_text_init(&lives, "assets//font//vcr.ttf", 12, FALSE);
    game_text_set_color_hex(&lives, 0xFFFFFF);
    game_text_set_shadow_color_hex(&lives, 0x383838);

}


void ui_update(void){
    int actualWeaponIndex = player.actual_weapon;
    WeaponConfig *actualWeapon = &player.weapons[actualWeaponIndex];



    //shoot_interval_bar.w = (actualWeapon->extra_counter / shoot_interval_bar.w) * 100;
}

void ui_render(void){
    game_text_draw(&lives, 50,50, "Lives: %d", player.lives);
    al_draw_filled_rectangle(shoot_interval_bar.x, shoot_interval_bar.y, shoot_interval_bar.x + shoot_interval_bar.w, shoot_interval_bar.y + shoot_interval_bar.h, al_map_rgb(255,0,0));
}
