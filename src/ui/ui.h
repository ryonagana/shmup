#ifndef UI_HEADER
#define UI_HEADER
#include <stdio.h>

void ui_start(void);
void ui_update(void);
void ui_render(void);

#endif // _UI_HEADER_
